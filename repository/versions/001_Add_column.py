from migrate import *
from sqlalchemy import *
from sqlalchemy.sql.schema import *
from sqlalchemy.types import *

def upgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    table = Table('space', meta)
    column0 = Column('size', BigInteger)
    column1 = Column('size', Integer)
    drop_column(column1,table)
    column0.create(table)

def downgrade(migrate_engine):
    meta = MetaData(bind=migrate_engine)
    table = Table('searchqueries', meta)
    column0 = Column('size', Integer)
    column1 = Column('size', BigInteger)
    drop_column(column1,table)
    column0.create(table)