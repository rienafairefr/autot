#!/usr/bin/env python
from migrate.versioning.shell import main
import six

if __name__ == '__main__':
    main(debug='False', six=six)
