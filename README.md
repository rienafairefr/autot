autotplex
=========

It's my first python project, hopefully it'll work for others.
I found useful to automate the download of episodes for shows watched via Plex. 
Disk space limitations, with this script I can keep on hands only a few episodes for any given show,
and once I watch an episode the script finds the download for the episode after, launches the download, 
name it, put it into plex, ready to go.
There is also a work-in-progress web interface via a Flask server.


HOWTO:

First, structure your Plex Library:
  en_something for shows in english
  fr_something for shows in french
  en-fr_something for shows in english subtitled in French
  etc.

    python autot/setup.py install

  This might create a blank autot.conf config file, you need to input values that make sense for your config in it.
  There is the possibility of getting emails from the program (when an episode is added, when a crash occurs), put smtp server/user/pass for that
  Config options should be pretty self-explanatory
  By default, the script can't delete files (old watched episodes)
  By default, output to stdout is rather verbose
Then just run it with

    python autot/run.py

or

    python -m autot.run

There is other functionalities, readme to be expanded.
