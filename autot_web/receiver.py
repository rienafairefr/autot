import zmq
from datetime import datetime

context = zmq.Context()
socket = context.socket(zmq.SUB)
# We can connect to several endpoints if we desire, and receive from all.
socket.bind('tcp://127.0.0.1:2341')

# We must declare the socket as of type SUBSCRIBER, and pass a prefix filter.
# Here, the filter is the empty string, wich means we receive all messages.
# We may subscribe to several filters, thus receiving from all.
socket.setsockopt(zmq.SUBSCRIBE, '')
socket.setsockopt(zmq.LINGER, 0)
while True:
    [address, msg] = socket.recv_multipart()
    print datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S:%f') + ' ' + address + ' : ' + msg
