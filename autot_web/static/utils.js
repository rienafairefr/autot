function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}

compact=function(obj){
    if (!obj) return "";
    if (SNNENN(obj)){
        return obj.lang+ '_ '+ obj.seriesname+ " - "+SNNENN(obj);
    }else{
        return obj.lang+ '_ '+ obj.seriesname;
    }
};
SNNENN=function(obj){
    if (!obj) return "";
    var eps=obj.episodenumbers;
    if (!eps) return null;
    if (eps.length==1){
        return "S"+padLeft(obj.seasonnumber,2,'0')+"E"+padLeft(eps[0],2,'0')
    }else{
        return "S"+padLeft(obj.seasonnumber,2,'0')+"E"+padLeft(eps[0],2,'0')+'-'+padLeft(eps[eps.length -1],2,'0')
    }
};


function bytesToSize(bytes, precision)
{
    var kilobyte = 1024;
    var megabyte = kilobyte * 1024;
    var gigabyte = megabyte * 1024;
    var terabyte = gigabyte * 1024;

    if ((bytes >= 0) && (bytes < kilobyte)) {
        return bytes + ' B';

    } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
        return (bytes / kilobyte).toFixed(precision) + ' KB';

    } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
        return (bytes / megabyte).toFixed(precision) + ' MB';

    } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
        return (bytes / gigabyte).toFixed(precision) + ' GB';

    } else if (bytes >= terabyte) {
        return (bytes / terabyte).toFixed(precision) + ' TB';

    } else {
        return bytes + ' B';
    }
}