# -*- coding=utf-8
import datetime
from autot.sqlA import KeyValue, Task, Status, EpisodeInfo, EpisodeNumbers, ShowInfo, CurrentShows
from autot.sqlA import CurrentEpisodes, ShowOrderType, ShowGroup, Space
from autot.sqlA import Group

from flask import Flask
from flask import render_template, request

from flask.ext.sqlalchemy import SQLAlchemy

import psutil
from sqlalchemy.orm.exc import NoResultFound

import autot
from autot import utils, store
from autot.run import inst
import json
from autot.config import *

from sqlalchemy import desc

app = Flask('autot_web')
app.config['SQLALCHEMY_DATABASE_URI'] = DBurl
# 'sqlite:////'+DBname
app.config['DEBUG'] = False
db = SQLAlchemy(app)

from flask import g


def timestamptohumanreadablerelative(timestamp):
    import time
    import datetime
    import dateutil.relativedelta

    t1 = datetime.datetime.fromtimestamp(int(timestamp))
    t2 = datetime.datetime.fromtimestamp(time.time())

    rd = dateutil.relativedelta.relativedelta(t2, t1)

    attrs = ['years', 'months', 'days', 'hours', 'minutes', 'seconds']
    nums = [0, 12, 29.5, 24, 60, 60]
    array = lambda delta: [getattr(delta, attr) for attr in attrs]
    human_readable = lambda delta: ['%d %s' % (getattr(delta, attr), getattr(delta, attr) > 1 \
                                               and attr or attr[:-1]) for \
                                    attr in attrs if getattr(delta, attr)]
    ar = array(rd)
    for idx, val in enumerate(ar):
        if val != 0 and idx != len(ar) - 1:
            return u' %d %s' % (
                (val + 1, attrs[idx]) if ar[idx + 1] >= 0.5 * nums[idx + 1] else (val, attrs[idx]))
    return u' %d %s' % (ar[-1], attrs[-1])


script = approot + 'run.py'
command = 'python ' + script


# def createcron():
# from crontab import CronTab
#
# cron = CronTab()
# job = cron.new(command=command)
# job.minute.every(1)
# job.enable()
# cron.write()
# return job


import flask

import tasks


@app.route('/launch', methods=['POST'])
def launch():
    # tasks.autot_run.delay()
    return 'blabla'


@app.route('/launch-f', methods=['POST'])
def launchf():
    tasks.autot_runforce.delay()
    return 'blablaf'


@app.route('/stopstream', methods=['POST'])
def stopstream():
    socket.close()
    return 'stopped'


import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt(zmq.SUBSCRIBE, '')
socket.setsockopt(zmq.LINGER, 0)
try:
    socket.bind('tcp://127.0.0.1:2340')
except zmq.error.ZMQError, e:
    socket.connect('tcp://127.0.0.1:2340')


@app.route('/stream')
def stream():
    try:
        def read():
            while True:
                # server
                [address, msg] = socket.recv_multipart()
                towrite = 'event:' + address + '\n'
                towrite += 'data:' + msg + '\n\n'
                yield towrite
                # time.sleep(1)
                # yield 'data:'+str(random.uniform(0, 1))+'\n\n'

        return flask.Response(read(), mimetype='text/event-stream')
    except Exception:
        socket.close()

        # return flask.Response(read(), mimetype='text/event-stream')


def startstopservice(value):
    import subprocess

    if value:
        p = subprocess.Popen(["service", "autot", "start"], stdout=subprocess.PIPE)
        out, err = p.communicate()
    else:
        p = subprocess.Popen(["service", "autot", "stop"], stdout=subprocess.PIPE)
        out, err = p.communicate()
    pass


@app.route('/addcurrentshow', methods=['POST'])
def addcurrentshow():
    pass


@app.route('/editcurrentshow', methods=['POST'])
def editcurrentshow():
    try:
        currentshowsi_id = int(request.form['id'])
        currentshow = db.session.query(CurrentShows).get(currentshowsi_id)
        if currentshow is None:
            return 'Not a CurrentShow', 400
        newcurrentshowvalue = request.form['value']
        spl = newcurrentshowvalue.split('_')
        if spl[0] in langs:
            tvdbinst = tvdb_instance[spl[0]]
            seriesname = ('_'.join(spl[1:])).strip()
            newshow = tvdbinst[seriesname]
            si = store.get_one_or_create(db.session, ShowInfo, lang=spl[0], seriesname=newshow.data['seriesname'])
            db.session.delete(currentshow)
            newcurrentshow = store.get_one_or_create(db.session, CurrentShows, si=si)
            db.session.commit()
        else:
            return 'Not a valid language', 400
        return flask.jsonify({'id': newcurrentshow.si_id, 'value': newcurrentshow.si.compact()})
    except ValueError, e1:
        return 'Not a valid CurrentShow ID', 400
    except tvdb_api.tvdb_shownotfound, e2:
        return 'TV Show not found in TVDB', 400


@app.route('/langs', methods=['GET'])
def getlangs():
    return flask.jsonify({'value': langs})


@app.route('/switchservice', methods=['POST'])
def switchservice():
    if runningservice():
        startstopservice(True)
    else:
        startstopservice(False)

        # from crontab import CronTab
        #
        # cron = CronTab()
        # try:
        # j = cron.find_command(command).next()
        # except StopIteration:
        # j = createcron()
        # j.enable(not j.is_enabled())
        # cron.write()
        # return 'True' if findcronjob().is_enabled() else 'False'


@app.route('/runningservice', methods=['POST', 'GET'])
def runningservice():
    import subprocess

    p = subprocess.Popen(["ps", "-a"], stdout=subprocess.PIPE)
    out, err = p.communicate()
    if 'autot' in out:
        return True
    return False
    # return 'True' if findcronjob().is_enabled() else 'False'


from sqlalchemy.orm import joinedload_all


def gettreespace():
    return db.session.query(Space). \
        options(joinedload_all("children", "children",
                               "children", "children")). \
        filter(Space.id == "root"). \
        first()


@app.route('/statuses')
def getstatuses():
    statuses = db.session.query(Status).order_by(desc(Status.time)).all()
    output = []
    for st in statuses:
        d = {'id': st.id}
        if st.ei:
            d['ei_id'] = st.ei_id
            d['eiorsi'] = st.ei.todict()
        if st.si:
            d['si_id'] = st.si_id
            d['eiorsi'] = st.si.todict()
        d['time'] = st.time.isoformat()
        d['status'] = st.status
        output.append(d)
    return flask.Response(json.dumps(output), mimetype='application/json')


@app.route('/tasks')
def gettasks():
    tasks = db.session.query(Task).order_by(desc(Task.time)).limit(10).all()
    output = []
    for t in tasks:
        d = {'time': t.time.isoformat()}
        if t.ei:
            d['ei'] = t.ei.todict()
        if t.si:
            d['si'] = t.si.todict()
        d['task'] = t.task
        output.append(d)
    return flask.Response(json.dumps(output), mimetype='application/json')


@app.route('/currentshows')
def getcurrentshows():
    shows = db.session.query(CurrentShows).all()
    output = []
    for sh in shows:
        d = {'si_id': sh.si_id, 'si': sh.si.todict(), 'unwatched': sh.unwatched, 'eilast_id': sh.eilast_id}
        if sh.eilast_id:
            d['eilast'] = sh.eilast.todict()
        else:
            d['eilast'] = None
        d_episodes = []
        eps = sh.episodes
        for ep in eps:
            d0 = {'id': ep.ei_id, 'type': ep.type}

            # d0['present']=inst.plex_interaction.isepisode_inplex(ep.ei) or inst.isepisode_downloading(ep.ei)

            d0.update(ep.ei.todict())
            d_episodes.append(d0)
        d['episodes'] = d_episodes
        output.append(d)
    return flask.Response(json.dumps(output), mimetype='application/json')


@app.route('/kv/<id>')
def getkv(id):
    retour = db.session.query(KeyValue.value).filter(KeyValue.id == 'running').scalar()
    return flask.Response(json.dumps(retour), mimetype='application/json')


@app.route('/EpisodeInfo')
def getepisodeinfo():
    id = request.args.get('id')
    type = request.args.get('type')
    try:
        ei = db.session.query(EpisodeInfo).filter(EpisodeInfo.id == id).one()
        eins = [x[0] for x in db.session.query(EpisodeNumbers.episodenumber).filter(EpisodeNumbers.ei == ei).all()]
        if type == 'compact':
            return flask.Response(json.dumps(ei.compact()), mimetype='application/json')
        else:
            return flask.Response(json.dumps({'lang': ei.lang,
                                              'seriesname': ei.seriesname,
                                              'seasonnumber': ei.seasonnumber,
                                              'episodenumbers': eins}), mimetype='application/json')
    except NoResultFound:
        return flask.Response('No EpisodeInfo for this id')


@app.route('/removeshow', methods=['POST'])
def removeshow():
    si_id = request.form['id']
    obj = db.session.query(CurrentShows).get(si_id)
    if obj:
        for currentepisode in obj.episodes:
            filepaths = json.loads(currentepisode.filepath)
            for filepath in filepaths:
                if os.path.exists(filepath):
                    os.remove(filepath)

        db.session.delete(obj)
        db.session.flush()
        db.session.commit()
        return flask.Response('OK done')
    else:
        return flask.Response('No CurrentShow with this id')


# @app.route('/ShowInfo')
# def getshowinfo():
# id=request.args.get('id')
# try:
# si = db.session.query(ShowInfo).filter(ShowInfo.id==id).one()
# if type=='compact':
# return flask.Response(json.dumps(ei.compact()),mimetype='application/json')
# else:
# return flask.Response(json.dumps({'lang':si.lang,
# 'seriesname':si.seriesname}),mimetype='application/json')
# except NoResultFound:
# return flask.Response('No ShowInfo for this id')

# @app.route('/removevalue',methods=['POST'])
# def removevaluetablename():
# tablename=request.form['tablename']
# id=store.string2id(request.form['id'])
# if store.hastable(tablename) and store.hasvalue(tablename,id):
# store.removevalue(tablename,id)



@app.route('/space')
def getspace():
    tree = gettreespace()
    if tree:
        tree = tree.todict()
        usage = psutil.disk_usage('/')
        data = {'tree': tree, 'usedpercent': usage.percent,
                'totalSpace': usage.total,
                'freeSpace': usage.free, }
        return flask.Response(json.dumps(data), mimetype='application/json')
    else:
        return ''


@app.route("/")
def flask_status():
    return render_template('autot.html', data=getdata('dict'))


@app.route("/username")
def returnusername():
    return inst.get_username()


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=False, port=32701)
