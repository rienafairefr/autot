# -*- coding=utf-8
import zmq
import logging

context = zmq.Context()
# s0=context.socket(zmq.PUB)
# s0.setsockopt(zmq.LINGER,0)
# s0.connect('tcp://127.0.0.1:2340')
#self.sockets.append(s0)
s = context.socket(zmq.PUB)
s.setsockopt(zmq.LINGER, 0)
s.connect('tcp://127.0.0.1:2341')

string = u'this is a gréat message'

logger = logging.getLogger('AutoTorrent')
import time

while True:
    logger.debug(string)
    #s.send_multipart(["msg", bytearray(string, 'utf-8')])
    time.sleep(3)