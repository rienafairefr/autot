#!/usr/bin/env python
from migrate.versioning.shell import main
from autot.config import DBurl
import six

if __name__ == '__main__':
    main(url=DBurl, debug='False', six=six)
