from setuptools import setup
from distutils.command.build_py import build_py
import os
from autot.config import generateblank


class MyBuildPy(build_py):
    def run(self):
        # honor the --dry-run flag
        if not self.dry_run:
            # mkpath is a distutils helper to create directories
            self.mkpath(os.path.join(self.build_lib, 'db'))
            import sqlite3

            sqlite3.connect(os.path.join(self.build_lib, 'db', 'autot.db'))

            f = open(os.path.join(self.build_lib, 'autot.conf'), 'w')
            f.write(generateblank())

        # distutils uses old-style classes, so no super()
        build_py.run(self)


setup(
    name='autotplex',
    version='0.2',
    long_description=__doc__,
    py_modules=['autot', 'autot_web'],
    include_package_data=True,
    zip_safe=False,
    install_requires=['lxml', 'psutil', 'tvnamer', 'py-plex', 'python-crontab', 'flask',
                      'transmissionrpc', 'tvdb_api', 'configparser', 'shelljob', 'feedparser'],
    cmdclass={'build_py': MyBuildPy}
)

