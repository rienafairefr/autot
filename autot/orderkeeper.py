import os
import random
from sqlalchemy.orm.exc import NoResultFound
from autot import Session
from config import tvdb_instance, approot, langs
from parser import parse_episodeinfo
from sqlA import Order, Group, ShowOrderType, ShowGroup
import store
from store import addifnotexists
from utils import stc
import logging

logger = logging.getLogger('AutoTorrent')
session=Session()
class KeepOrders():
    def __init__(self):
        self.groups = None
        self.lines = None
        self.indices = None

    def getnextep(self,eilast):
        session=Session.object_session(eilast)
        lang=eilast.lang
        showtitle=eilast.seriesname

        tvdbsearch = tvdb_instance[lang][showtitle]
        ncurrseas = eilast.seasonnumber
        ncurrep = max(map(lambda x:x.episodenumber,eilast.episodenumbers))
        tvdbseasons = tvdbsearch.values()
        nlast = len(tvdbseasons) - 1
        lastepindex_currseason = len(tvdbsearch[ncurrseas].values())

        if ncurrep + 1 <= lastepindex_currseason:
            # Download the next episode in the season
            episodeinfo=store.getei(session=session,lang=lang,seriesname=showtitle,seasonnumber=ncurrseas,episodenumbers = [ncurrep + 1])
        elif ncurrseas + 1 <= nlast:
            # Get the next season's premiere
            episodeinfo=store.getei(session=session,lang=lang,seriesname=showtitle,seasonnumber=ncurrseas+1,episodenumbers = [1])
        else:
            # No next episode possible, snif
            return
        return episodeinfo

    def treatorder(self):
        if not any(map(lambda x: os.path.exists(approot + x + '_orders'), langs)):
            return
        groups=set([])
        lines=[]
        indices=[]
        for lang in langs:
            if not os.path.exists(approot + lang + '_orders'):
                continue
            orders = open(approot + lang+'_orders', 'r').read().split('\n')
            for idx, val in enumerate(orders):
                if idx < len(orders) - 1 and orders[idx] != '':
                    if orders[idx].strip() != '' and orders[idx + 1].strip() != '':
                        logger.debug(str((orders[idx], orders[idx + 1])))
                        ei1= parse_episodeinfo(orders[idx], lang,session=session)
                        ei2= parse_episodeinfo(orders[idx + 1], lang,session=session)
                        ei1.nextep_id=ei2.id
                        #ei2sn=compact_si(ei2.si)
                        si1=store.getsi(ei1)
                        si2=store.getsi(ei2)
                        t0=tuple({si1,si2})
                        if si1==si2:
                            continue
                        foundit=False
                        for g in groups:
                            if si1 in g or si2 in g:
                                foundit=True
                                groups.remove(g)
                                frz=frozenset(t0+tuple(g))
                                groups.add(frz)
                                if not g==frz:
                                    indices.pop(indices.index(tuple(g)))
                                    indices.append(tuple(frz))
                                    #indices[]
                                    #indices[tuple(frz)]=indices.pop(tuple(g))
                                    #lines[idxs[tuple(frz)]]=lines.pop(idxs[tuple(g)])

                                lines[indices.index(tuple(frz))].append(ei2)
                                break
                        if not foundit:
                            indices.append(t0)
                            lines.insert(len(indices)-1,[])
                            groups.add(frozenset(t0))
                        if not ei1 is None and not ei2 is None:
                            addifnotexists(session,Order,ei1=ei1,ei2=ei2)
                            #store.setorder(ei1,ei2)
                        else:
                            logger.debug(stc('Couldn\'t treat the order for ',orders[idx],' and ',orders[idx+1]))
        groups=list(groups)
        for idx,g in enumerate(groups):
            groups[idx]=list(g)
            for line_order,ei in enumerate(lines[idx]):
                addifnotexists(session,Group,group_id=idx,si=store.getsi(ei),ei=ei,line_order=line_order)
            for si in groups[idx]:
                addifnotexists(session,ShowOrderType,si=si,type="grouped")
                addifnotexists(session,ShowGroup,si=si,group_id=idx)

        for lang in langs:
            if not os.path.exists(approot + lang + '_random'):
                continue
            rands = open(approot + lang+'_random', 'r').read().split('\n')
            for idx, val in enumerate(rands):
                if val != '':
                    ei11= parse_episodeinfo(rands[idx]+' S01E01', lang,session=session)

                    if not ei11 is None:
                        si11=store.getsi(ei11)
                        addifnotexists(session,ShowOrderType,si=si11,type="random")
                    else:
                        logger.debug(stc('Couldn\'t treat the random order for ',val))
        session.commit()
    def getlines(self):
        return session.query(Group).all()
    def getg(self,si):
        gs = self.getgroups()
        m = map(lambda x: x.si, gs)
        if si in m:
            g = list(gs[m.index(si)])
        else:
            g = [si]
        return g
    def getgroups(self):
        return session.query(ShowGroup).all()

    #def treatgroups(self):
    #    allorders = store.getall('orders')#cur.execute("SELECT * FROM orders WHERE 1").fetchall()
    #
    #    l = []
    #    for id in allorders:
    #        val=allorders[id]
    #        if val!='random':
    #            l.append(reversecompact_ei(val))
    #    g0 = set([])
    #    while True:
    #        currei = l[0]
    #        g = set([])
    #        while True:
    #            print(currei)
    #            pre=len(l)
    #            g.add(currei.seriesname)
    #            newei = self.getnext_order(currei)
    #            if newei is None: break
    #            l.remove(currei)
    #            if newei == currei:
    #                logger.error(stc('the order database has a problem, next is equal to current for ' ,currei))
    #                break
    #            currei = newei
    #            post=len(l)
    #            if pre==post:
    #                pass
    #        g0.add(frozenset(g))
    #        if len(l) == 0: break
    #    g0 = list(g0)
    #    return g0

    def israndom(self, si):
        if session.query(ShowOrderType.type).filter(ShowOrderType.si==si).count()!=0:
            return session.query(ShowOrderType.type).filter(ShowOrderType.si==si).scalar()=='random'
        else :
            return False

            #con = sqlite3.connect(DBname)
            #cur = con.cursor()

            #val2=store.getvalue('orders',si)
            #val2 = cur.execute("SELECT value FROM orders WHERE id=?", (compact_si(si),)).fetchall()
            #return val2 is not None and val2=='random'
    def getnext_order(self, episodeinfo):
        session=Session.object_session(episodeinfo)
        if not episodeinfo: return None
        si=store.getsi(episodeinfo)
        if self.israndom(si):
            lang=episodeinfo.lang
            mat=tvdb_instance[lang][episodeinfo.seriesname].search('')
            sel=random.choice(mat)
            return store.getei(session=session,lang=lang,seriesname=episodeinfo.seriesname,
                               episodenumbers=[int(sel['episodenumber'])],seasonnumber=int(sel['seasonnumber']))
        else:
            try:
                return session.query(Order).filter(Order.ei1==episodeinfo).one().ei2
            except NoResultFound:
                return None