from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
import tvnamer
from tvnamer.tvnamer_exceptions import DataRetrievalError, UserAbort
from autot import Session
from sqlA import *
import store
from tvnamer.utils import FileParser
from config import *
fp=FileParser('')
from sqlalchemy.inspection import inspect
def parse_episodeinfo(string, lang,session=None):
    if session is None:
        raise Exception('we should give a session to the parser')
    try:
        tvnamer.config.Config['language'] = lang
        episodeinfo=eifromstring(session,exceptions_fromdl(string),lang)
        return episodeinfo
    #except (tvnamer.tvnamer_exceptions.EpisodeNotFound, tvnamer.tvnamer_exceptions.ShowNotFound,
    #        tvnamer.tvnamer_exceptions.InvalidFilename, tvnamer.tvnamer_exceptions.SeasonNotFound):
    except tvnamer.tvnamer_exceptions.InvalidFilename, e1:
        return None

def exceptions_fromdl(name):
    import re

    name2 = re.compile('hostages[ \.]us', re.IGNORECASE).sub('Hostages', name)
    name2 = re.compile('scandal[ \.]us|scandal', re.IGNORECASE).sub('Scandal (2012)', name2)
    name2 = re.compile('r&i', re.IGNORECASE).sub('Rizzoli & Isles', name2)
    name2 = re.compile('\[www[ \.]cpasbien[ \.][a-z]{1,3}\]', re.IGNORECASE).sub('', name2)
    name2 = re.compile('\[www[ \.]omgtorrent[ \.][a-z]{1,3}\]', re.IGNORECASE).sub('', name2)
    #name2 = re.compile('\[www[\ \.]cpasbien[\ \.]com\]', re.IGNORECASE).sub('', name2)
    #name2 = re.compile('\[www[\ \.]cpasbien[\ \.]pe\]', re.IGNORECASE).sub('', name2)
    name2 = re.compile('ghwhisp', re.IGNORECASE).sub('Ghost Whisperer', name2)
    # if name2!=name:
    #	logger.info(stc('Changed ',nameorig,' to ',name))
    return name2

import re
def detectlanguage(toparsestring,eiparsed):
    return None
    tvdbshow=tvdb_instance[eiparsed.lang][eiparsed.seriesname]
    if re.search('VOSTFR',toparsestring,re.IGNORECASE) and re.search('FASTSUB',toparsestring,re.IGNORECASE):
        return 'en-fr'
    if re.search('tv ru',toparsestring,re.IGNORECASE):
        return 'ru'
    if re.search('swesub',re.IGNORECASE):
        return 'en-sw'
    pass


def eifromstring(session,toparsestring,lang,cache=True):
    try:

        queryresult=session.query(EpisodeInfoCache).filter(EpisodeInfoCache.str==toparsestring).one()
        eitest= queryresult.ei
        if eitest is None:
            session.delete(queryresult)
            session.commit()
            raise NoResultFound()
        else:
            return eitest
    except NoResultFound:
        fp.path=toparsestring
        ei0 = fp.parse()
        try:
            ei0.populateFromTvdb(tvdb_instance[lang])
        except (DataRetrievalError,AttributeError,UserAbort):
            return None
        if type(ei0)==tvnamer.utils.NoSeasonEpisodeInfo:
            return None


        #showinfo=store.get_one_or_create(session,ShowInfo,lang=lang,seriesname=ei0.seriesname)
        try:
            #episodeinfo=store.get_one_or_create(session,EpisodeInfo,)
            episodeinfo=store.getei(session,lang=lang,seriesname=ei0.seriesname,seasonnumber= ei0.seasonnumber,episodenumbers=ei0.episodenumbers)
            #for eins in ei0.episodenumbers:
                #store.addobjectifnotexists(session,EpisodeNumbers(ei=episodeinfo,episodenumber=eins))
            #print(episodeinfo.compact())
        except MultipleResultsFound:

            listeiids=session.query(EpisodeInfo.id).filter(EpisodeInfo.lang==lang,EpisodeInfo.seriesname==ei0.seriesname,EpisodeInfo.seasonnumber== ei0.seasonnumber).all()
            for ei_id in [r for (r,) in listeiids]:
                en=session.query(EpisodeNumbers).filter(EpisodeNumbers.ei_id==ei_id).all()
                if map(lambda x:x.episodenumber,session.query(EpisodeNumbers).filter(EpisodeNumbers.ei_id==ei_id).all())==ei0.episodenumbers:
                    return en[0].ei
            pass

        lang2=detectlanguage(toparsestring,episodeinfo)

        store.addifnotexists(session,EpisodeInfoCache,str=toparsestring,ei=episodeinfo)
        return episodeinfo