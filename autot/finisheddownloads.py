import fnmatch
import os
import shutil
import tvnamer
from watchdog.events import FileSystemEventHandler
from autot import Session
from parser import parse_episodeinfo, exceptions_fromdl
import plexint
from sqlA import Task, Status
import store
import utils
from utils import stc
import logging

logger = logging.getLogger('AutoTorrent')

class FinishedDownloadsHandler:
    def __init__(self,lang):
        self.lang=lang
    def treat_downloaded_files(self):
        matches = []
        pathlang=os.path.expanduser("~/downloads/finished_" + self.lang)
        sizepathlang=utils.get_size(pathlang)
        for root, dir, files in os.walk(pathlang):
            for filematch in fnmatch.filter(files, "*"):
                matches.append(os.path.join(root, filematch))
        logger.debug('Treating downloaded files for language ' + self.lang)
        for filematch in matches:
            self.treat_downloaded_file(filematch)
        if len(matches) == 0:
            logger.debug('No files to treat for language ' + self.lang)
    def treat_downloaded_file(self,filematch):
        session=Session()
        #logger.debug('Treating '+filematch)
        haschanged=False
        base = os.path.basename(filematch)
        dire = os.path.dirname(filematch)

        season = ("Saison" if self.lang == 'fr' else "Season")

        try:
            if base.endswith('.part'): return
            if os.path.getsize(filematch) > 20000000:
                episodeinfo = parse_episodeinfo(exceptions_fromdl(base), self.lang,session=session)
                if episodeinfo is None :
                    logger.debug(stc('file ',filematch,'doesn''t match an episode'))
                    return
                    #compactepisodeinfo = compact_ei(episodeinfo)

                newdir = os.path.expanduser(
                    "~/Plex/" + self.lang + "_Shows/" + episodeinfo.seriesname + "/" + season + " " + str(
                        episodeinfo.seasonnumber))
                if not os.path.exists(newdir):
                    os.makedirs(newdir, mode=0777)

                compnolang = ''.join(episodeinfo.compact().split('_')[1:]).strip()
                newfile = newdir + "/" + compnolang + '_' + base
                if not os.path.exists(newfile):
                    logger.info(stc(episodeinfo, ' Treating downloaded file'))
                    logger.info(stc('Moving ', filematch))
                    logger.info(stc('to ', newfile))

                    shutil.move(filematch, newfile)
                    os.link(newfile, filematch)
                    store.addifnotexists(session,Task,ei=episodeinfo,task='Added to Plex from')
                    haschanged = True
                    store.addifnotexists(session,Status,ei=episodeinfo,status='in Plex')
                    store.removefromstatus(episodeinfo,'downloading')
            if haschanged:
                plexint.PlexInteraction().refresh_plex()
        except tvnamer.tvnamer_exceptions.InvalidFilename:
            logger.debug(stc('File not recognized ', base))


class FinishedFilesEventHandler(FileSystemEventHandler):
    def __init__(self,lang):
        self.handler=FinishedDownloadsHandler(lang)

    def on_created(self, event):
        self.handler.treat_downloaded_file(event.src_path)
        super(FinishedFilesEventHandler, self).on_created(event)