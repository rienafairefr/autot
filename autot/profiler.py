import cProfile,StringIO
import pstats
from autot.run import inst

pr = cProfile.Profile(builtins=False)
pr.enable()
inst.main()
pr.disable()

s = StringIO.StringIO()
sortby = 'cumul'
# calls, cumul, file, line, module, name, nfl, pcalls, std, time

ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
pr.dump_stats(file="Autot.profile")
print s.getvalue()