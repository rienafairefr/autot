from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
import json
from utils import stc, CustomTypeDecoder,CustomTypeEncoder
from autot import Session
from sqlA import KeyValue,Status
#approot = os.path.realpath(os.path.dirname(os.path.realpath(__file__)))
#DBname = approot + '/db/autot.db'
#con = sqlite3.connect(DBname,check_same_thread=False)
#con.row_factory = sqlite3.Row
#cur = con.cursor()

from sqlA import Task

def getall(tablename):
    dictoutput= {}
    ids=getallids(tablename)
    for id in ids:
        dictoutput[id2string(id)]=getrow(tablename,id)
    return dictoutput

def getallids(tablename):
    #cur.execute('SELECT PRIMARY KEY FROM %s WHERE 1;' % tablename)
    #listid = list(cur.fetchall())
    #for idx,l in enumerate(listid):
    #    listid[idx]=string2id(l[0])
    return Session().query(KeyValue.id).all()
    #return listid

import logging
logger=logging.getLogger('AutoTorrent')

def value2string(value):
    try:
        value2=json.dumps(value)
    except TypeError,e:
        try:
            value2=json.dumps(value,cls=CustomTypeEncoder,ensure_ascii=True)
        except TypeError:
            pass
    return value2
def setaddvaluetodict(tablevalue,id, keydict,value):
    I=id2string(id)
    value2=value2string(value)



def get_tree(base, dest_dict):
    dest_dict = { 'id': base.title, 'size': base.size }
    children = base.children
    if children:
        dest_dict['children'] = {}
        for child in children:
            get_tree(child, dest_dict)
    else:
        return

from sqlA import Space
def setspace(id,parent_id,size):
    session=Session()
    if id=='root':
        addifnotexists(session,Space,id=id,parent_id='',size=size)
    #try:
    #cont_id=session.query(Space.id).filter(Space.id==container_id).one()
    addifnotexists(session,Space,id=id,parent_id=parent_id,size=size)
    # except NoResultFound:
    #     pass
    # except MultipleResultsFound:
    #
    #     passaddifnotexists(session,SpaceKeyValue,id=id,container_id=None,size=size)

def setvalue(id,value):
    addifnotexists(Session(),KeyValue,id=id,value=value)

def getrow(tablename,id):
    return Session().query(KeyValue).filter(KeyValue.id==id).first()



def getvalue(id):
    try:
        val=Session().query(KeyValue).filter(id==id).first().value
        return val
    except NoResultFound:
        return None

def gettime(tablename,id):
    try:
        ggg=getrow(tablename,id)
        if 'time' in ggg.keys():
            return ggg['time']
    except ValueError:
        return None

def id2string(id):
    if type(id)==str or type(id)==unicode:
        return id
    else:
        return json.dumps(id,cls=CustomTypeEncoder,ensure_ascii=True)
def string2id(id):
    try:
        return json.loads(id,object_hook=CustomTypeDecoder)
    except ValueError,e:
        return id

def hasvalue(tablevalue,id):
    return Session().query(KeyValue).filter(KeyValue.id==id).count()>0

    #vals=cur.execute("SELECT id FROM %s WHERE id=?;" % tablevalue, (id2string(id),)).fetchone()
    #return vals is not None

from sqlA import Status
def removefromstatus(ei,value):
    try:
        Session().query(Status).filter(and_(Status.ei==ei,Status.status==value)).delete()
    except NoResultFound:
        pass
#def testschemas():        pass

#    for tablename in tables():
#
#        if not hastable(tablename):
#            createtable(tablename)
#            continue
#        #cursor=con.execute('SELECT * FROM '+name+' WHERE 1')
#        #columnsbefore= list(map(lambda x: x[0], cursor.description))
#        #cursor=con.execute('SELECT * FROM dummy WHERE 1')
#        #columnsafter= list(map(lambda x: x[0], cursor.description))
#        #if not columnsbefore==columnsafter:
#        con.execute("DROP TABLE IF EXISTS 'dummy'")
#        con.commit()
#        if not tablename in schemas.keys():
#            createtable('dummy')
#        else:
#            createtable('dummy',schema=schemas[tablename])
#        if not getschema(tablename).replace('%s('%tablename,'dummy(')==getschema('dummy'):
#
#            con.execute('DROP TABLE IF EXISTS %s' % tablename)
#            con.commit()
#            createtable(tablename)
#            logger.info('reset db '+tablename+' done')
#
#def getschema(name):
#    val=cur.execute("SELECT sql FROM sqlite_master WHERE name='" + name + "'").fetchone()
#    if val:
#        return val[0]
#    else:
#        return ''

#def tables():
#    val=cur.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall()
#    if val is not None:
#        return map(lambda x:x[0],val)
#    else:
#        return []
#def hastable(name):
#    val = cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='" + name + "'").fetchone()
#    return val is not None


def get_one_or_create(session,
                      model,
                      create_method='',
                      create_method_kwargs=None,
                      **kwargs):
    try:
        return session.query(model).filter_by(**kwargs).one()
    except NoResultFound:
        kwargs.update(create_method_kwargs or {})
        created = getattr(model, create_method, model)(**kwargs)
        try:
            session.add(created)
            session.commit()
            return created
        except IntegrityError:
            session.rollback()
            #return session.query(model).filter_by(**kwargs).one()

def addifnotexists(session,
                      model,
                      create_method='',
                      create_method_kwargs=None,
                      **kwargs):
    try:
        val=session.query(model).filter_by(**kwargs).one()
        return True
    except NoResultFound:
        kwargs.update(create_method_kwargs or {})
        created = getattr(model, create_method, model)(**kwargs)
        try:
            session.add(created)
            session.commit()
            return False
        except IntegrityError:
            session.rollback()

def addobjectifnotexists(session,
                      object,):
    try:
        session.add(object)
        session.commit()
    except Exception,e:
        pass
        session.rollback()
from sqlalchemy import and_
from sqlA import EpisodeInfo,EpisodeNumbers,ShowInfo

def getsi(episodeinfo):
    return get_one_or_create(Session.object_session(episodeinfo),ShowInfo,lang=episodeinfo.lang,seriesname=episodeinfo.seriesname)
def getei(session,lang,seriesname, seasonnumber, episodenumbers):
    result1=session.query(EpisodeInfo).filter(and_(EpisodeInfo.lang==lang,EpisodeInfo.seriesname==seriesname,EpisodeInfo.seasonnumber==seasonnumber)).all()
    #episodeinfo=get_one_or_create(session,EpisodeInfo,si=si,seasonnumber=seasonnumber)

    for ei in result1:
        if map(lambda x:x.episodenumber,ei.episodenumbers)==episodenumbers:
            return ei
    episodeinfo=EpisodeInfo(lang=lang,seriesname=seriesname,seasonnumber=seasonnumber)
    addobjectifnotexists(session,episodeinfo)
    for eins in episodenumbers:
        addobjectifnotexists(session,EpisodeNumbers(ei_id=episodeinfo.id,episodenumber=eins))
    return episodeinfo

