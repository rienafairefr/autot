import urllib2
import urllib
import mechanize
import lxml.html
from lxml import etree

class AutoTSearchPlugin:
    name = 'kickass.to'
    lang = ['en', 'en-fr']
    type= 'query'

    def get(self, data):
        query=data['query']
        if data['lang'] == 'en-fr':
            query += ' VOSTFR'
        matches = []



        # e=encodeURIComponent(text).replace(/!/g,'%21').replace(/'/g,'%27').replace(/\(/g,'%28').replace(/\)/g,'%29').replace(/\*/g,'%2A');
        qreplaced=query.replace('!', '%21').replace('\'', '%27').replace('(', '%28').replace(')', '%29').replace('*', '%2A')
        query = urllib.quote(qreplaced.encode('utf-8'))
        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.set_handle_equiv(True)
        br.set_handle_redirect(True)
        br.set_handle_gzip(True)
        br.set_handle_referer(True)
        br.addheaders = [('User-agent', 'Chrome')]
        for p in xrange(1,30):
            url = 'http://kickass.to/usearch/{0}/{1}'.format(query,p)
            try:
                response=br.open(url)
            except (mechanize.HTTPError,mechanize.URLError) as e:
                if len(matches)>0:
                    return matches
                else:
                    return None
            tree = etree.HTML(br.response().read())

            xp0=tree.xpath('//tr[starts-with(@id,"torrent_")]')

            for el0 in xp0:
                it=el0.iter()
                val={}
                for el in it:
                    if 'class' in el.attrib:
                        if el.attrib['class']=='cellMainLink':
                            val['title'] = ''.join([t for t in el.itertext()])
                        if el.attrib['class']=="green center":
                            val['seed'] = int(el.text)
                        if el.attrib['class'].startswith('imagnet icon') and 'href' in el.attrib:
                            val['magnet'] = el.attrib['href']
                        if el.attrib['class'].startswith('idownload icon') and 'href' in el.attrib:
                            val['torrent'] = el.attrib['href']
                vk=val.keys()
                if not ('title' in vk and 'seed' in vk): continue
                if not ('torrent' in vk or 'magnet' in vk): continue
                if not 'magnet' in vk and 'torrent' in vk :
                    if val['seed']==0:continue
                    retrieve=br.retrieve(val['torrent'])
                    fh=open(retrieve[0],'rb')
                    torr=fh.read()
                    fh.close()
                    matches.append({'title': val['title'], 'torrent': torr, 'seeders': val['seed']})
                    continue
                matches.append({'title': val['title'], 'torrent': val['magnet'], 'seeders': val['seed']})
        return matches