import urllib
import urllib2
import libxml2
import time


class AutoTSearchPlugin:
    lang = ['fr', 'en-fr']
    name = 't411.me'
    type= 'query'

    setupdata={}

    def setup(self, data):
        if not 'lastsetup' in data or not data['lastsetup'] or (int(time.time()) - data['lastsetup']) > 1000:
            request = urllib2.Request("http://api.t411.me/auth" \
                , data=urllib.urlencode({"username": data['username'], "password": data['password']}) \
                , headers={"User-Agent": data['useragent']})
            result = urllib2.urlopen(request, timeout=1).read()
            import json
            datac=data.copy()

            self.setupdata['token'] = json.loads(result)['token']
            datac['lastsetup'] = str(int(time.time()))
            return datac
        else:
            return data

    def get2(self,data):
        query=data['query']
        matches=[]
        #Trying to work through scraping
        import mechanize
        import cookielib

        # Browser
        br = mechanize.Browser()

        # Cookie Jar
        cj = cookielib.LWPCookieJar()
        br.set_cookiejar(cj)

        # Browser options
        br.set_handle_equiv(True)
        br.set_handle_gzip(True)
        br.set_handle_redirect(True)
        br.set_handle_referer(True)
        br.set_handle_robots(False)

        # Follows refresh 0 but not hangs on refresh > 0
        br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

        # User-Agent (this is cheating, ok?)
        br.addheaders = [('User-agent', 'Chrome')]

        # The site we will navigate into, handling it's session
        br.open('http://www.t411.me/users/login')

        # Inspect name of the form
        for f in br.forms():
            if f.attrs['id']=='loginform':
                br.form=f
                br.form['login'] = data['username']
                br.form['password'] = data['password']
                br.submit()
                break
        for f in br.forms():
            if f.attrs['id']=='bar-search':
                br.form=f
                br.form['search']=query.encode('utf-8')
                br.submit()
                break
        p=0
        while True:
            p += 1
            from lxml import etree

            tree = etree.HTML(br.response().read())

            r0=tree.xpath('//a[@class="switcher alignright"]/preceding-sibling::a')
            r1=tree.xpath('//a[@class="ajax nfo"]')
            val={}
            for el in r1:
                if 'href' in el.attrib:
                    arr=el.attrib['href'].split('=')
                    if len(arr)==2:
                        id=arr[1]
                        # http://www.t411.me/torrents/download/?id=4901542
                        try:
                            torr=br.retrieve('http://www.t411.me/torrents/download/?id='+id,timeout=5)
                        except urllib2.URLError,e:
                            continue
                        fh=open(torr[0],'rb')
                        val['torrent']=fh.read()
                        fh.close()
                        parent=el.getparent().getparent()
                        for el2 in parent.iter():
                            if el2 in r0 and 'title' in el2.attrib:
                                val['title']=el2.attrib['title']
                            if 'class' in el2.attrib and el2.attrib['class']=='up' and el2.tag=='td':
                                val['seed']=int(el2.text)
                        vk=val.keys()
                        if not ('title' in vk and 'seed' in vk and 'torrent' in vk): continue
                        matches.append({'title': val['title'], 'torrent': val['torrent'], 'seeders': val['seed']})

            r=tree.xpath('//a[@title="Suivant Page"]')
            if p>30 : break
            if len(r)==0:
                break
            else:
                if 'href' in r[0].attrib:br.open(r[0].attrib['href'])
        return matches

    def get(self, data):
        lang=data['lang']
        query=data['query']
        if lang == 'fr':
            query += " -VOSTFR -FASTSUB -VOSTF"
        if lang == 'en-fr':
            query += " VOSTFR"
        query += " -720p -1080p"

        matches = []
        # t411 {"uid":"96102539","token":"96102539:161:d51f8e3bc11a0263bf3619106f1459ad"}

        result0 = urllib2.urlopen(urllib2.Request("https://api.t411.me/torrents/search/avatar",
                                                  headers={"Authorization": str(data['token']),
                                                           "Content-Type": 'application/x-www-form-urlencoded',
                                                           "User-agent": str(data['useragent'])})).read()
        if not result0:
            return self.get2(data)
            raise Exception('We couldn\'t contact the t411 API, even with the basic example query')
        query.replace(' ','+')
        urlquery = "http://api.t411.me/torrents/search/" + urllib.quote_plus(query.encode('utf-8'))
        request = urllib2.Request(urlquery, headers={"Authorization": data['token'],
                                                     "Content-Type": 'application/x-www-form-urlencoded',
                                                     "User-agent": data['useragent']})
        result = urllib2.urlopen(request, timeout=5).read()

        import json

        try:
            loaded = json.loads(result)
        except ValueError:
            print('query URL [' + urlquery + '] for search [' + query + '] had a problem')
            return None

        seq = [int(x['seeders']) for x in loaded['torrents'] if x['seeders'] != "0"]
        #for res in loaded['torrents']:
        #onetorrent=json.loads(res)
        if seq:
            res = next((item for item in loaded['torrents'] if int(item["seeders"]) == max(seq)), None)
            id = res['id']

            requestdl = urllib2.Request("https://api.t411.me/torrents/download/"+id, headers={"Authorization" : "96102539:161:d51f8e3bc11a0263bf3619106f1459ad","User-agent":"Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0"})
            resulttorrentfile=urllib2.urlopen(requestdl, timeout = 1).read()
            try:
                dic=json.loads(resulttorrentfile)
                return self.get2(data)
            except ValueError,ev:

            #print('torrent file')
                matches.append({'title': res['name'], 'torrent': resulttorrentfile, 'seeders': max(seq)})
        return matches