import httplib


class AutoTSearchPlugin:
    name = 'cpasbien'
    lang = ['fr', 'en-fr']
    type= 'query'

    def get(self, data):
        query=data['query']
        lang=data['lang']
        import urllib2
        import urllib

        matches = []
        url = 'http://www.cpasbien.pe/recherche/'
        if lang == 'fr':
            values = {'champ_recherche': query}
        elif lang == 'en-fr':
            values = {'champ_recherche': (query + ' VOSTFR')}
        else:
            return None

        str_value = {}
        for k, v in values.iteritems():
            str_value[k] = unicode(v).encode('utf-8')
        data = urllib.urlencode(str_value)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        the_page = response.read()
        from lxml import etree

        tree = etree.HTML(the_page)

        r = tree.xpath('//a[@class="lien-rechercher"]')
        for el in r:
            href = el.attrib['href']

            req = urllib2.Request(href)
            response2 = urllib2.urlopen(req)
            the_page2 = response2.read()

            tree2 = etree.HTML(the_page2)
            el0 = tree2.xpath('//a[contains(@href,".torrent")]')
            if len(el0) >= 1:
                el0 = el0[0]
            dlhref = el0.attrib['href']
            from urlparse import urljoin

            dlhref = urljoin(href, dlhref)
            torrentreq = urllib2.Request(dlhref)
            torrentresponse = urllib2.urlopen(torrentreq)
            torrentresult = torrentresponse.read()

            dlseed = int(tree2.xpath('//span[@class="seed_ok"]')[0].text)

            el1 = tree2.xpath('//a[contains(@href,".torrent")]/text()')
            if len(el1) >= 2:
                title = el1[1]
            else:
                return None
            matches.append({'title': title, 'torrent': torrentresult, 'seeders': dlseed})
        return matches