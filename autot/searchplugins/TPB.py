import timeit
class AutoTSearchPlugin:
    name = 'thepiratebay.org'
    lang = ['en', 'en-fr']
    type= 'query'

    def get(self, data):
        query=data['query']
        lang=data['lang']
        if lang == 'en-fr':
            query += ' VOSTFR'
        matches = []
        from tpb import TPB
        from tpb import CATEGORIES, ORDERS
        import urllib2
        import socket
        #tpb

        #query=query+" -\"nl subs\" -subtitulado -720p -1080p -FASTSUB -swesub"

        t = TPB('https://thepiratebay.org')


        # code you want to evaluate

        search = t.search(query=query, category=CATEGORIES.VIDEO.TV_SHOWS)
        search.order(ORDERS.SEEDERS.DES)

        for i in range(1, 5):
            spi=search.page(i)
            for x in spi:
                if x.seeders != 0:
                    if lang != 'en-fr' and ('vostfr' in x.title.lower()
                                            or 'fastsub' in x.title.lower()): continue
                    if x.torrent_link:
                        try:
                            #print(x.torrent_link)
                            if x.torrent_link.startswith('//'):
                                x.torrent_link = 'http:' + x.torrent_link

                                result = urllib2.urlopen(x.torrent_link, timeout=3).read()
                            else:
                                continue

                        except socket.timeout as etime:
                            print('Timeout while running TPB torrent link fetch')
                            continue
                        except Exception as e:
                            print('Bad URL from the API or something: ' + x.torrent_link)
                            continue

                    else:
                        result = x.magnet_link
                    if result:
                        matches.append({'title': x.title, 'torrent': result, 'seeders': x.seeders})
        return matches
