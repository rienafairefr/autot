import mechanize
from lxml import etree
import difflib
from autot.run import parse_episodeinfo

class AutoTSearchPlugin:
    name = 'eztv'
    lang = ['en']
    type= 'ei'

    def get(self, data):
        matches=[]
        ei=data['episodeinfo']

        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.set_handle_equiv(True)
        br.set_handle_redirect(True)
        br.set_handle_gzip(True)
        br.set_handle_referer(True)
        br.addheaders = [('User-agent', 'Chrome')]

        urlshows = 'https://eztv.it/showlist/'

        try:
            response=br.open(urlshows)

            tree = etree.HTML(br.response().read())

            ashows=tree.xpath("//a[@class='thread_link']")
            showstitles=map(lambda x:x.text.lower(),ashows)

            if ei.seriesname.lower() in showstitles:
                idx= showstitles.index(ei.seriesname.lower())
                response=br.open(ashows[idx].attrib['href'])
                tree = etree.HTML(br.response().read())

                episodes=tree.xpath("//[@class='epinfo']")

                for ep in episodes:
                    eiparsed= parse_episodeinfo(ep.attrib['title'],lang=self.lang)
                    if eiparsed==ei:

                        tr=tree.xpath("//tr/descendant::*[@class='epinfo']")

                        #/ancestor::*
                        matches.append([])
                showstitles=map(lambda x:x.text.lower(),ashows)

            else:
                differences=map(lambda x:difflib.SequenceMatcher(None, x.lower(), ei.si.seriesname.lower()).ratio(),showstitles)






            pass
        except (mechanize.HTTPError,mechanize.URLError) as e:
            pass

        return matches