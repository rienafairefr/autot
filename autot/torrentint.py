import logging
import os
import transmissionrpc
from config import cp, dlprefix, seedratio
logger=logging.getLogger('AutoTorrent')
from sqlA import TorrentStatus
import store
from autot import Session


class TorrentInteraction:
    logger = logging.getLogger('AutoTorrent')

    def __init__(self):
        try:
            tcaddress = cp.get('Transmission', 'address', fallback='localhost')
            tcuser = cp.get('Transmission', 'user', fallback='')
            tcpassword = cp.get('Transmission', 'password', fallback='')

            self.transmissionserver = transmissionrpc.Client(address=tcaddress, user=tcuser, password=tcpassword)
            self.tors = self.transmissionserver.get_torrents()
        except Exception:
            logger.error('Error in setting up transmissionrpc, is transmission-daemon service running? ')
            exit()

    def hash_downloading_episodes(self):
        session=Session()
        hash=''
        from run import parse_episodeinfo
        for tor in self.tors:
            dldir=tor._fields['downloadDir'].value
            if dldir.startswith(dlprefix):
                lang=dldir.split(dlprefix)[1]
                for key, f in tor.files().iteritems():
                    if f['selected']:
                        ei=parse_episodeinfo(f['name'],lang,session)
                        if ei is not None:
                            #session=Session.object_session(ei)
                            if f['completed']==f['size']:
                                #store.addifnotexists(session,TorrentStatus,ei=ei,status='Finished')
                                hash+='Finished'
                                ftor=dldir+'/'+f['name']
                                if os.path.exists(ftor) and os.stat(ftor).st_nlink==1:
                                    hash+='Treated'
                            else:
                                hash+='Downloading'

    def treat_downloading_episodes(self):
        session=Session()
        from run import parse_episodeinfo
        for tor in self.tors:
            dldir=tor._fields['downloadDir'].value
            if dldir.startswith(dlprefix):
                lang=dldir.split(dlprefix)[1]
                for key, f in tor.files().iteritems():
                    if f['selected']:
                        ei=parse_episodeinfo(f['name'],lang,session=session)
                        if ei is not None:
                            if f['completed']==f['size']:
                                store.addifnotexists(session,TorrentStatus,ei=ei,status='Finished')
                                ftor=dldir+'/'+f['name']
                                if os.path.exists(ftor) and os.stat(ftor).st_nlink==1:
                                    store.addifnotexists(session,TorrentStatus,ei=ei,status='Treated')
                            else:
                                store.addifnotexists(session,TorrentStatus,ei=ei,status='Downloading')

    def remove_badpartfiles(self):
        tors = self.transmissionserver.get_torrents()
        for tor in tors:
            if tor._fields['downloadDir'].value.startswith(dlprefix):
                for key, f in tor.files().iteritems():
                    partfilepath=tor._fields['downloadDir'].value+'/'+f['name']+'.part'
                    if not f['selected'] and os.path.exists(partfilepath):
                        os.remove(partfilepath)
    def remove_seeded_torrents(self):
        tc = transmissionrpc.Client(address='localhost', user="matthieu", password="Tptq93x")
        tors = tc.get_torrents()
        for tor in tors:
            # logger.debug('finishedtoseed torrent:'+str(tor)+' ?')
            if tor._fields['downloadDir'].value.startswith(dlprefix) and tor.ratio >= seedratio:
                tostop = True
                for key, f in tor.files().iteritems():
                    if f['selected'] and f['completed'] < f['size']:
                        tostop = False
                if tostop:
                    tc.remove_torrent(tor.id, delete_data=True)
                    #store.addifnotexists(session,Task,)
                    #self.runner.settask(stc('Removed finished-to-seed ', str(tor)))