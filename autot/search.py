import os
import sys
import urllib2
import hashlib
from config import approot,getdata, setdata, cp,tvdb_instance
import logging
logger=logging.getLogger('AutoTorrent')
from utils import dummy_none, stc
import cProfile
import time
from datetime import datetime
from datetime import timedelta
from sqlA import SearchQuery
import json
import fnmatch
import libtorrent as lt
class TorrentSearcher:
    plugins = {}
    pluginbynames = {}
    def __init__(self):
        self.loadplugins()
    def cleaningcache(self):
        matches=[]
        for root, dir, files in os.walk(approot+'cache/torrent/'):
            for filematch in fnmatch.filter(files, "*"):
                    matches.append(os.path.join(root, filematch))
        for f in matches:
            if not (f.endswith('.cache') or f.endswith('.torrent')):os.remove(f)
            if f.endswith('.cache'):
                try:
                    fh=open(f,'r')
                    obj=json.loads(fh.read())
                    fh.close()
                    if type(obj) is list and len(obj)==2:
                        if datetime.now()-datetime.fromtimestamp(obj[0])>timedelta(days=3):
                            os.remove(f)
                    else:
                        os.remove(f)
                except Exception,e:
                    os.remove(f)
            if f.endswith('.torrent'):
                ctime=os.stat(f).st_ctime
                if datetime.now()-datetime.fromtimestamp( os.stat(f).st_ctime)>timedelta(weeks=1):
                    os.remove(f)
    def getfrom(self,sq, cache=True):
        plugin=self.pluginbynames[sq.pluginname]
        data=sq.data

        if '-d' in sys.argv:
            return getfromnoexcept(plugin,data,cache)
        try:
            try:
                return getfromnoexcept(plugin,data,cache)
            except urllib2.URLError, e:
                logger.debug('URL error in ' + plugin.name)
                return None
        except Exception as e:
            logger.debug('Unknown error  searching ' + plugin.name)
            if e.__doc__:
                logger.debug('Exception doc: ' + e.__doc__)
            if e.message:
                logger.debug('Exception message: ' + e.message)
            import traceback

            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            logger.debug('Exception info:\n' + ''.join('!! ' + line for line in lines))
            #traceback.print_exc(file=sys.stdout)
            return None
    def loadplugins(self):
        import glob
        modulessearch = glob.glob(approot + 'searchplugins/*.py')
        # Loads search plugins
        matches = []
        foundhandler = False

        for mod2 in modulessearch:
            searchplugin = loadsearchplugin(mod2,expected_class='AutoTSearchPlugin')
            if searchplugin:
                self.pluginbynames[searchplugin.name]=searchplugin
                for l in searchplugin.lang:
                    if not l in self.plugins.keys():
                        self.plugins[l]={}
                    if not searchplugin.type in self.plugins[l].keys():
                        self.plugins[l][searchplugin.type]=[]
                    self.plugins[l][searchplugin.type].append(searchplugin)


    def add_searchquery_ei(self, ei):
        #eiorsitoget=store.get_one_or_create(session,Episode_or_Show_Info,create_method='create',eiorsi=ei)
        session=Session.object_session(ei)
        if 'ei' in self.plugins[ei.lang].keys():
            for searchplugin in self.plugins[ei.lang]['ei'] :
                store.addifnotexists(session,SearchQuery,create_method='create',data={'episodeinfo_id':ei.id},pluginname=searchplugin.name,eiorsi=ei)
    def add_searchquery_query(self, query, eiorsi,getit=True):
        session=Session.object_session(eiorsi)
        #eiorsitoget=store.get_one_or_create(session,Episode_or_Show_Info,create_method='create',eiorsi=eiorsi)
        lang=eiorsi.lang
        for searchplugin in self.plugins[lang]['query']:
            store.addifnotexists(session,SearchQuery,create_method='create',eiorsi=eiorsi,pluginname=searchplugin.name,data={'lang':lang,'query':query},getit=getit)
        if lang == 'fr':
            for searchplugin in self.plugins['en']['query']:
                store.addifnotexists(session,SearchQuery,create_method='create',eiorsi=eiorsi,pluginname=searchplugin.name,data={'lang':lang,'query':'FRENCH '+query},getit=getit)
                store.addifnotexists(session,SearchQuery,create_method='create',eiorsi=eiorsi,pluginname=searchplugin.name,data={'lang':lang,'query':'TRUEFRENCH '+query},getit=getit)
    def add_searchqueries_random(self, eiorsi,getit=True,addother=True):
        #eiorsitoget=store.get_one_or_create(session,Episode_or_Show_Info,create_method='create',eiorsi=eiorsi)
        Seasons=tvdb_instance[eiorsi.lang][eiorsi.seriesname]
        nseasons=len(Seasons)

        for name in self.getalternatives(eiorsi.seriesname):
            self.add_searchquery_query(stc(name, ' S0'), eiorsi,getit=getit)
            self.add_searchquery_query(name, eiorsi,getit=getit)
            for ns in xrange(0,nseasons-1):
                nns = ('%02d' % ns)
                snn = "S" + nns
                self.add_searchquery_query(stc(name, ' \"Season ', nns, '\"'), eiorsi,getit=getit)
                if eiorsi.lang == 'fr':
                    self.add_searchquery_query(stc(name, ' \"Saison ', nns, '\"'), eiorsi,getit=getit)
                self.add_searchquery_query(stc(name, ' ', snn), eiorsi,getit=getit)
        if addother:
            #self.addothersearchqueries(eiorsi)
            pass
        pass

    def getalternatives(self,seriesname):
        alternatives = []
        try:
            alternatives = filter(None, cp.get('SEARCH ALT', seriesname).split('\n'))
        except Exception:
            pass
        alternatives.insert(0,seriesname)
        return alternatives
    def add_searchqueries_episode(self, eitoget,addother=True):
        logger.info(stc('Adding search queries for episode ', eitoget.compact()))
        matches = []

        seriesname=eitoget.seriesname
        season=eitoget.seasonnumber
        nns = ('%02d' % eitoget.seasonnumber)
        for ens in eitoget.episodenumbers:
            en=ens.episodenumber
            nne = ('%02d' % en)
            snn = "S" + nns
            enn = "E" + nne

            for name in self.getalternatives(seriesname):
                self.add_searchquery_query(stc(name, " ", snn, enn), eiorsi=eitoget)
                self.add_searchquery_query(stc(name, " ", ('%01d' % season), "x", nne), eiorsi=eitoget)
                self.add_searchquery_query(stc(name, " ", snn, " ", enn), eiorsi=eitoget)
                self.add_searchquery_query(stc(name, ' \"Season ', nns, '\"'), eiorsi=eitoget)
                if eitoget.lang == 'fr':
                    self.add_searchquery_query(stc(name, ' \"Saison ', nns, '\"'), eiorsi=eitoget)
                self.add_searchquery_query(stc(name, ' ', snn), eiorsi=eitoget)
                self.add_searchquery_query(stc(name, ' S0'),eiorsi= eitoget)
                self.add_searchquery_query(name, eiorsi=eitoget)
        if addother:
            #self.addothersearchqueries(eitoget)
            pass

    def addothersearchqueries(self, eiorsi):

        session=Session.object_session(eiorsi)
        logger.info(stc('Adding other search queries for ', eiorsi.compact()))
        tvdbseries=tvdb_instance[eiorsi.lang][eiorsi.seriesname]
        for seasidx,seas in tvdb_instance[eiorsi.lang][eiorsi.seriesname].items():
            for epidx,ep in seas.items():
                ei=store.getei(session,eiorsi.lang,eiorsi.seriesname,seasidx,[epidx])
                self.add_searchqueries_episode(ei,addother=False)
                pass
        pass


def loadsearchplugin(filepath,expected_class):
    import imp

    class_inst = None


    mod_name, file_ext = os.path.splitext(os.path.split(filepath)[-1])

    py_mod = None
    if file_ext.lower() == '.py':
        py_mod = imp.load_source(mod_name, filepath)

    elif file_ext.lower() == '.pyc':
        py_mod = imp.load_compiled(mod_name, filepath)

    if hasattr(py_mod, expected_class):
        class_inst = getattr(py_mod, expected_class)()

    return class_inst

import store
import base64

from autot import Session
#session=Session()
from sqlA import TorrentCache, EpisodeInfo

def getfromnoexcept(plugin,data,cache):
    session=Session()
    hashid=hashlib.md5(stc(plugin.name,data).encode('utf-8')).hexdigest()

    if session.query(TorrentCache).filter(TorrentCache.hashid==hashid).count()>0:
        gotit=True
        #store.removevalue('torrents',hashid)
        resultfirst=session.query(TorrentCache).filter(TorrentCache.hashid==hashid).first()
        if resultfirst.title=='NO TORRENT FOUND':
            return None
        results=session.query(TorrentCache).filter(TorrentCache.hashid==hashid).all()
        matches=[]
        for res in results:
            if datetime.utcnow()-res.time>timedelta(days=7):
                session.delete(res)
                continue

            TORR=res.torrent
            if not (TORR.startswith('file://') or TORR.startswith('magnet:')):
                #torrfile=approot + 'cache/torrent/' + TORR[10:] + '.torrent'
                #if not os.path.exists(torrfile):
                session.delete(res)
                continue
            matches.append({'title': res.title, 'torrent': TORR, 'seeders': res.seeders})
        if matches:
            return matches

    logger.debug(stc('Search in ' , plugin.name , ' for data ' , data))
    # setup called each time. Plugin's role to check if really needed to setup
    # (e.g. not asking for new auth tokens every minute)
    data.update(getdata(plugin.name))

    if hasattr(plugin, 'setup'):
        data2 = plugin.setup(data)
        setdata(plugin.name, data2)
        matches = plugin.get(data2)
    else:
        matches = plugin.get(data)
    if cache:
        if matches:
            matchescache=matches[:]
            for idx,el in enumerate(matchescache):
                torrent=el['torrent']
                if not torrent.startswith('magnet:'):
                    info = lt.torrent_info(torrent,len(torrent))
                    hexadecimal = str(info.info_hash())
                    try:
                        os.makedirs(approot+'cache/torrent')
                    except os.error:
                        pass
                    torrfile=approot+'cache/torrent/'+hexadecimal+'.torrent'
                    if not os.path.exists(torrfile):
                        with open(torrfile,'w+') as f:
                            f.write(torrent)
                    matchescache[idx]['torrent']='file://'+torrfile
            cachelist = matchescache
            for match in cachelist:
                store.addifnotexists(session,TorrentCache,hashid=hashid,title=match['title'],torrent=match['torrent'],seeders=str(match['seeders']))
        else:
            cachelist = []
            store.addifnotexists(session,TorrentCache,hashid=hashid,title='NO TORRENT FOUND')
    if matches:
        logger.info(stc(data['lang'] ,'_ Found ',str(len(matches)),' matches in ',plugin.name,' for data ',data))
        return matches
    else:
        logger.debug(stc(data['lang'],'_ No matches in ',plugin.name,' for data ',data))
        return None

