# -*- coding=utf-8
import os
import logging
import json

logger = logging.getLogger('AutoTorrent')

def get_size(start_path='.'):
    total_size = 0
    seen = {}
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            try:
                stat = os.stat(fp)
            except OSError:
                continue

            try:
                seen[stat.st_ino]
            except KeyError:
                seen[stat.st_ino] = True
            else:
                continue

            total_size += stat.st_size

    return total_size

def bytes2human(n):
    # http://code.activestate.com/recipes/578019
    # >>> bytes2human(10000)
    # '9.8K'
    # >>> bytes2human(100001221)
    # '95.4M'
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n

class ShowInfo():
    lang = ''
    seriesname = ''

    def __init__(self, dictinput):
        self.__dict__ = dictinput
    def compact(self):
        return stc(self.lang, '_ ', self.seriesname)
    def __hash__(self):
        return (self.lang, self.seriesname).__hash__()

    def __str__(self):
        return self.compact()
    def __eq__(self, other):
        if other is None: return False
        if type(self) == type(other):
            return self.compact() == other.compact()
        else:
            return False


class EpisodeInfo():

    si=None
    seasonnumber = None
    episodenumbers = [None]

    def compact(self):
        part1 = stc(self.lang, '_ ', self.seriesname, " - S", ('%02d' % self.seasonnumber))

        if len(self.episodenumbers) == 1:
            part2 = ('E%02d' % self.episodenumbers[0])
        else:
            part2 = stc('E%02d' % self.episodenumbers[0], '-', 'E%02d' % self.episodenumbers[-1])
        return stc(part1, part2)

    def contains(self, ei):
        return (
            True if self == ei else (self.lang == ei.lang and self.seriesname==ei.seriesname) and self.seasonnumber == ei.seasonnumber
            and all([(x in self.episodenumbers) for x in ei.episodenumbers]))

    def __eq__(self, other):
        if other is None: return False
        if type(self) == type(other):
            return self.compact() == other.compact()
        else:
            return False

    def __hash__(self):
        return (self.lang, self.seriesname, self.seasonnumber, tuple(self.episodenumbers)).__hash__()

    def __str__(self):
        return self.compact()

import re
recei=re.compile("(?P<lang>[a-z]{2}|[a-z]{2}-[a-z]{2})_ (?P<seriesname>.*) - S(?P<seasonnumber>[0-9]{2})E(?P<episodenumbers>[0-9]{2})")
recsi=re.compile("(?P<lang>[a-z]{2}|[a-z]{2}-[a-z]{2})_ (?P<seriesname>.*)")
def reversecompact_si(string):
    dict=recsi.match(string).groupdict()
    if not all(map(lambda x: x in ['lang','seriesname'],dict.keys())):
        raise Exception('Bad String to reversecompactify')
    return ShowInfo.from_dict(dict)


def reversecompact_ei(string):
    dict=recei.match(string).groupdict()
    if not all(map(lambda x: x in ['lang','seriesname','seasonnumber','episodenumbers'],dict.keys())):
        raise Exception('Bad String to reversecompactify')
    return EpisodeInfo(dict)

def stc(*arg):
    list1=[]
    for el in list(arg):
        if type(el)==str or type(el)==unicode:
            list1.append(el)
        else:
            if type(el)==list:
                list1.append(stc('[',','.join(map(lambda x:x.__str__(),el)),']'))
            else:
                list1.append(el.__str__())
    return u''.join([(el.decode('utf-8') if type(el) == str else el) for el in list1])


def dummy_none(obj):
    return [] if (obj is None) else obj

TYPES = {'EpisodeInfo': EpisodeInfo,
          'ShowInfo': ShowInfo}
class CustomTypeEncoder(json.JSONEncoder):
    """A custom JSONEncoder class that knows how to encode core custom
    objects.

    Custom objects are encoded as JSON object literals (ie, dicts) with
    one key, '__TypeName__' where 'TypeName' is the actual name of the
    type to which the object belongs.  That single key maps to another
    object literal which is just the __dict__ of the object encoded."""

    def default(self, obj):
        if isinstance(obj, tuple(TYPES.values())):
            key = '__%s__' % obj.__class__.__name__
            return { key: obj.__dict__ }
        return json.JSONEncoder.default(self, obj)


def CustomTypeDecoder(dct):
    if len(dct) == 1:
        type_name, value = dct.items()[0]
        type_name = type_name.strip('_')
        if type_name in TYPES:
            if 'from_dict' in dir(TYPES[type_name]):
                return TYPES[type_name].from_dict(value)
            else:
                return TYPES[type_name](value)
    return dct