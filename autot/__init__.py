from sqlalchemy.pool import NullPool, SingletonThreadPool

__all__ = ['run','plexint','utils','config','search','torrentint']

import sqlite3
from config import DBurl

from sqlalchemy import engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.orm import scoped_session

#conn=sqlite3.connect(DBname)
#conn.close()
#engine=engine.create_engine('sqlite:////'+DBname,echo=False,connect_args={'check_same_thread': False}).connect()
engine=engine.create_engine(DBurl,echo=False).connect()

#Session = sessionmaker(bind=engine,)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)



