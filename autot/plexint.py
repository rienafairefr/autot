from datetime import datetime, timedelta
import os
import time
import urllib
import logging
from store import hasvalue, setvalue, getvalue
import store
from utils import stc
import configparser
from config import *
from sqlA import ShowInfo, CurrentEpisodes, SearchQuery, Task
from sqlA import CurrentShows
from autot import Session
import json
logger = logging.getLogger('AutoTorrent')

class PlexInteraction():

    def querynew(self,key,cache=True):
        if cache and key in self.querycache.keys():
            return self.querycache[key]
        else:
            element=self.plexserver.queryold(key)
            self.querycache[key]=element
            return element

    def __init__(self):
        self.logger = logging.getLogger('AutoTorrent')
        self.querycache={}
        from plex.server import Server
        cp = configparser.ConfigParser()
        cp.readfp(codecs.open(conf_file, "r", "utf8"))
        try:
            plexurl = cp.get('Plex', 'address', fallback='localhost')
            plexport = cp.getint('Plex', 'port', fallback=32400)

            self.plexserver = Server(plexurl, plexport)  # insert your Plex server's ip address
            try:
                shows=self.plexserver.library.shows
            except UnboundLocalError:
                raise Exception
            self.plexserver.queryold=self.plexserver.query
            self.plexserver.query=self.querynew
        except Exception as e:
            self.logger.error('Error in setting up plex-server')
            exit()

    def treat_cleanup_plex2(self, section, show, lang):
        session=Session()
        nwatched = 0
        eps = []
        for season in show.seasons:
            thisseason = sorted(season.episodes, key=lambda x: x.index)
            for i, ep in enumerate(thisseason):
                thisseason[i].index2 = (season.index, ep.index)
            eps += thisseason
        eps = sorted(eps, key=lambda x: x.index2)

        leneps = len(eps)
        for idx, ep in enumerate(eps):
            nextep = eps[(idx + 1) % leneps]

            # try:
            #     response = urllib2.urlopen("http://%s:%d%s" % (
            #         self.plexserver.address, self.plexserver.port, ep.key))
            #     nextresponse = urllib2.urlopen("http://%s:%d%s" % (
            #         self.plexserver.address, self.plexserver.port, nextep.key))
            # except urllib2.URLError:
            #     return

            element = self.plexserver.query(ep.key)
            nextelement = self.plexserver.query(nextep.key)

            viewed = self.isviewed(element,ep)
            nextviewed = self.isviewed(nextelement,nextep)

            if viewed and nextviewed and not idx==len(eps)-1 :
                epfile=urllib.unquote(ep.file)
                if os.path.exists(epfile) and os.stat(epfile).st_nlink == 1:
                    # the Plex file is the only one remaining
                    self.logger.debug(stc('deleting ',epfile,'  ...'))
                    os.remove(epfile)
                    self.refresh_plex()
                    while True:
                        try:
                            self.plexserver.query(ep.key,cache=False)
                        except UnboundLocalError:
                            break
                        time.sleep(5)
                    self.logger.debug('the above HTTP 404 is normal, disregard')
                    store.addifnotexists(session,Task,task=stc('deleted ',epfile))

    def loop_plex(self):
        session=Session()
        #self.runner.torrent_interaction.tors = self.runner.torrent_interaction.transmissionserver.get_torrents()
        shs=[]
        for section in self.plexserver.library.shows:
            spl = section.title.split('_')
            if len(spl) != 2:
                self.logger.error('unrecognizable section')
                continue
            lang = spl[0]
            if not 'nonAuto' in section.title:
                for show in section.getContent():
                    si= store.get_one_or_create(session,ShowInfo,lang=lang,seriesname=show.title)
                    logger.debug('show %s'%si.compact())
                    self.treat_plex_episodes(si,show)
                    if candelete:
                        self.treat_cleanup_plex2(section, show,lang)

    def refresh_plex(self,force=False):
        import urllib2

        host = 'localhost'
        source_type = ['movie', 'show']  # Valid values: artist (for music), movie, show (for tv)
        base_url = 'http://localhost:32400/library/sections'
        if force:
            refresh_url = '%s/%%s/refresh?force=1' % base_url
        else:
            refresh_url = '%s/%%s/refresh' % base_url
        for section in self.plexserver.library.shows:
            url = refresh_url % section.key
            x = urllib2.urlopen(url, timeout=1)
        self.logger.info('Called Refresh Plex Scanner')


        #session.query(CurrentShows).filter(CurrentShows.si==si_in).one()

        #gid=session.query(ShowGroup.group_id).filter(ShowGroup.si==si_in).scalar()
        #session.query(Group.ei).filter(and_(ShowGroup.group_id==gid,ShowGroup.si==si_in)).one()
        #session.query(Group)
        #for section in self.plexserver.library.shows:
        #    spl = section.title.split('_')
        #    if len(spl) != 2: continue
        #    lang = spl[0]
        #    if lang == si.lang:
        #        for show in section.getContent():
        #            pass
        #            if show.title == si.seriesname:
        #                shss = show.seasons
        #                currseason = show.getSeason(shss[-1].index)
        #                ncurrsea = currseason.index
        #                eps = currseason.episodes
        #                ncurrep = eps[-1].index
        #
        #                return session.query(EpisodeInfo).filter(si==si
        #                    **{'lang': lang, 'seriesname': show.title, 'seasonnumber': ncurrsea,
        #                     'episodenumbers': [ncurrep]})
        #logger.error('there was an error in looping Plex, a show apparently disappeared '+ showtitle)
    showscache={}
    def getshow(self,si):
        if si in self.showscache.keys():
            return self.showscache[si]
        else:
            for section in self.plexserver.library.shows:
                spl = section.title.split('_')
                if len(spl) != 2: continue
                lang = spl[0]
                if lang == si.lang:
                    for show in section.getContent():
                        if show.title == si.seriesname:
                            self.showscache[si]=show
                            return show


    def getnextep(self,eilast):
        session=Session.object_session(eilast)
        lang=eilast.lang
        showtitle=eilast.seriesname

        tvdbsearch = tvdb_instance[lang][showtitle]
        ncurrseas = eilast.seasonnumber
        ncurrep = max(map(lambda x:x.episodenumber,eilast.episodenumbers))
        tvdbseasons = tvdbsearch.values()
        nlast = len(tvdbseasons) - 1
        lastepindex_currseason = len(tvdbsearch[ncurrseas].values())

        if ncurrep + 1 <= lastepindex_currseason:
            # Download the next episode in the season
            episodeinfo=store.getei(session=session,lang=lang,seriesname=showtitle,seasonnumber=ncurrseas,episodenumbers = [ncurrep + 1])
        elif ncurrseas + 1 <= nlast:
            # Get the next season's premiere
            episodeinfo=store.getei(session=session,lang=lang,seriesname=showtitle,seasonnumber=ncurrseas+1,episodenumbers = [1])
        else:
            # No next episode possible, snif
            return
        return episodeinfo

    def treat_space(self,show,lang):
        session=Session()
        totalsize=0
        for season in show.seasons:
            eps=season.episodes
            for idx,ep in enumerate(eps):
                totalsize+=os.stat(urllib.unquote(ep.file)).st_size
        si=store.get_one_or_create(session,ShowInfo,**{'lang':lang,'seriesname':show.title})
        store.setspace(si.compact(),'Plex',totalsize,session=session)

    #def treat_nextepisodes_plex(self, si):
    #    #si=utils.ShowInfo({'lang' : lang,'seriesname':show.title})
    #    downloading_episodes=self.runner.torrent_interaction.downloading_episodes(si)
    #    unwatched_episodes=self.plex_unwatched_episodes(si)
    #    numunwatched=len(set(downloading_episodes+unwatched_episodes))
    #    if numunwatched < keepunwatched:
    #        uwe2=self.plex_unwatched_episodes(si)
    #        try:
    #            groups = self.runner.orderkeeper.getgroups()
    #            lines = self.runner.orderkeeper.getlines()
    #            indices = self.runner.orderkeeper.getindices()
    #            m= map(lambda x: si in x, groups)
    #            if any(m):
    #                idx=m.index(True)
    #                g02=indices[idx]
    #                tg0=tuple(g02)
    #                g = list(g02)
    #                l = lines[idx]
    #                m1 = map(lambda x: self.geteilastep_plex(x), g)
    #                lll=[]
    #                for idx2, x in enumerate(g):
    #                    if m1[idx2] in l:
    #                        lll.append(l.index(m1[idx2]))
    #                ei_last=l[max(lll)]
    #            else:
    #                ei_last= self.geteilastep_plex(si)
    #            if self.runner.orderkeeper.israndom(si):
    #                self.runner.getnext_episode_random(ei_last.si)
    #                return
    #            else:
    #                self.getnextep(ei_last)
    #        except tvdb_exceptions.tvdb_shownotfound, tvdb_exceptions.tvdb_seasonnotfound:
    #            self.logger.error(stc('Couldn\'t find TV show ' , si.seriesname , ', extracted from Plex in tvdb'))

    def isviewed(self,element,ep):
        videos=element.getiterator('Video')
        viewed1=all(map(lambda x:x.attrib['viewCount']>='1' if 'viewCount' in x.attrib else False,videos))
        viewed2 = ('viewCount' in element.attrib) and (element.attrib['viewCount'] >= '1')
        return any((viewed2,viewed1,ep.viewed))

    def treat_plex_episodes(self, si,show):
        session=Session.object_session(si)
        thiscurrentshow=store.get_one_or_create(session,CurrentShows,si=si)
        unw=0

        maxindex2=(0,0)
        maxep=None


        session.query(CurrentEpisodes).filter(CurrentEpisodes.si==si,CurrentEpisodes.currentshow==thiscurrentshow).delete()
        session.commit()
        indb=map(lambda x:x.ei,session.query(CurrentEpisodes).filter(CurrentEpisodes.si==si,CurrentEpisodes.currentshow==thiscurrentshow).all())

        for ns,seas in enumerate(show.seasons):
            for ep in seas.episodes:
                element=self.plexserver.query(ep.key)

                ei=store.getei(session=session,lang=si.lang,seriesname=si.seriesname,seasonnumber=seas.index,episodenumbers=[ep.index])
                if ei in indb:
                    indb.remove(ei)
                if (ep.index,seas.index)>maxindex2:
                    maxindex2=(ep.index,seas.index)
                    maxep=ei
                #store.addifnotexists(session,EpisodeNumbers,episodenumber=ep.index,ei=ei)
                # 1 fully viewed, 2 partially vieewd, 3 fully unwatched


                array0=[]
                def gothrough(element,array):
                    #array.append(element)
                    if element.attrib and 'file' in element.attrib:
                        array.append(element.attrib['file'])
                    for ch in element._children:
                        gothrough(ch,array)

                gothrough(element,array0)
                filepath=json.dumps(array0)

                if self.isviewed(element,ep):
                    store.addifnotexists(session,CurrentEpisodes,si=si,ei=ei,currentshow=thiscurrentshow,type="fully watched",filepath=filepath)
                else:
                    if ep.offset>0:
                        store.addifnotexists(session,CurrentEpisodes,si=si,ei=ei,currentshow=thiscurrentshow,type="partially watched",filepath=filepath)
                    else:
                        unw += 1
                        store.addifnotexists(session,CurrentEpisodes,si=si,ei=ei,currentshow=thiscurrentshow,type="fully unwatched",filepath=filepath)
        for ei in indb:
            session.query(CurrentEpisodes).filter(CurrentEpisodes.ei==ei,CurrentEpisodes.si==si,CurrentEpisodes.currentshow==thiscurrentshow).delete()

        thiscurrentshow.eilast=maxep
        thiscurrentshow.unwatched=unw
        session.flush()

    def plex_numepisodes(self, si):
        sections = {}
        for section in self.plexserver.library.shows:
            spl = section.title.split('_')
            if len(spl) != 2: continue
            lg = spl[0]
            if lg not in langs: continue
            if lg!=si.lang: continue
            for show in section.getContent():
                if si.seriesname == show.title:
                    return len(show.getAllEpisodes())

        self.logger.error(stc('Couldn\'t find TV show ',si.seriesname,', in Plex'))
        return 0


    def isepisode_inplex(self, episodeinfo):
        session=Session.object_session(episodeinfo)
        sections = {}
        for section in self.plexserver.library.shows:
            lang = ("fr" if ("FR" in section.title) else 'en')
            for show in section.getContent():
                for seas in show.seasons:
                    for ep in seas.episodes:
                        eiep = store.getei(session=session,lang=lang, seriesname=show.title,seasonnumber=seas.index,episodenumbers=[ep.index])
                        if eiep.contains(episodeinfo):
                            return True
        return False