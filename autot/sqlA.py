import sqlalchemy
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.sql.schema import ForeignKey, Table
from utils import stc
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.declarative import declarative_base

from autot import engine
from sqlalchemy.types import Integer, String, DateTime
from sqlalchemy.schema import Column
from sqlalchemy.orm import relationship, backref
from  sqlalchemy.sql.sqltypes import Boolean
from sqlalchemy.sql.sqltypes import BigInteger

Base = declarative_base(bind=engine)

class SeeninRSS(Base):
    __tablename__='seeninRSS'
    id=Column(String(),primary_key=True)

class ShowInfo(Base):
    __tablename__='showinfos'
    id=Column(Integer(),primary_key=True)
    lang = Column(String())
    seriesname = Column(String())

    def compact(self):
        return stc(self.lang, '_ ', self.seriesname)
    def __hash__(self):
        return (self.lang, self.seriesname).__hash__()
    def todict(self):
        return {'lang':self.lang,'seriesname':self.seriesname}
    def __str__(self):
        return self.compact()
    def __eq__(self, other):
        if other is None: return False
        if type(self) == type(other):
            return self.compact() == other.compact()
        else:
            return False
import json

class EpisodeNumbers(Base):
    __tablename__="episodenumbers"
    id=Column(Integer(),primary_key=True)
    ei_id=Column(Integer(),ForeignKey("episodeinfos.id"))
    ei=relationship("EpisodeInfo")
    episodenumber=Column(Integer())

class KeyValue(Base):
    __tablename__='kv'
    id=Column(String(),primary_key=True)
    value=Column(String())
import datetime

class Space(Base):
    __tablename__='space'
    id=Column(String(),primary_key=True)
    size=Column(BigInteger())

    parent_id = Column(String(), ForeignKey('space.id'))

    def todict(self):
        chdicts=[]
        for ch in self.children:
            chdicts.append(ch.todict())
        if not chdicts:
            return {'id':self.id,'size':self.size}
        else:
            return {'id':self.id,'size':self.size,'children':chdicts}

    children=relationship('Space',
                          # cascade deletions
                          cascade="all",

                          # many to one + adjacency list - remote_side
                          # is required to reference the 'remote'
                          # column in the join condition.
                          backref=backref("parent", remote_side='Space.id'),)

    # children will be represented as a dictionary
    # on the "name" attribute.
    # collection_class=attribute_mapped_collection('id'),)




class Task(Base):
    __tablename__='tasks'
    ei_id=Column(Integer(),ForeignKey('episodeinfos.id'),primary_key=True)
    ei=relationship('EpisodeInfo')
    si_id=Column(Integer(),ForeignKey('showinfos.id'),primary_key=True)
    si=relationship('ShowInfo')
    task=Column(String())
    time=Column(DateTime(),default=datetime.datetime.utcnow())


class CurrentEpisodes(Base):
    __tablename__='currentepisodes'
    ei_id=Column(Integer(),ForeignKey('episodeinfos.id'),primary_key=True)
    ei=relationship("EpisodeInfo")
    filepath=Column(String())
    currentshow=relationship("CurrentShows")
    currentshow_id=Column(Integer, ForeignKey('currentshows.si_id'))
    si_id = Column(Integer, ForeignKey('showinfos.id'))
    si= relationship("ShowInfo")
    type=Column(String())

class SearchQuery(Base):
    __tablename__='searchqueries'
    id=Column(Integer(),primary_key=True)
    ei_id=Column(Integer(),ForeignKey('episodeinfos.id'))
    ei=relationship("EpisodeInfo")
    si_id=Column(Integer(),ForeignKey('showinfos.id'))
    si=relationship("ShowInfo")
    getit=Column(Boolean,default=True)
    priority=Column(Integer,default=1)
    pluginname=Column(String())
    datastring=Column(String())

    @hybrid_property
    def eiorsi(self):
        if self.ei is not None:
            return self.ei
        if self.si is not None:
            return self.si

    @eiorsi.setter
    def eiorsi(self,value):
        if isinstance(value,EpisodeInfo):
            self.ei=value
        if isinstance(value,ShowInfo):
            self.si=value

    @hybrid_property
    def data(self):
        try:
            return json.loads(self.datastring)
        except TypeError:
            pass
    @data.setter
    def data(self,dd):
        self.datastring=json.dumps(dd)


class TorrentStatus(Base):
    __tablename__='torrentstatuses'
    id=Column(Integer(),primary_key=True)
    ei_id=Column(Integer(),ForeignKey('episodeinfos.id'))
    ei=relationship('EpisodeInfo')
    si_id=Column(Integer(),ForeignKey('showinfos.id'))
    si=relationship('ShowInfo')

    @hybrid_property
    def eiorsi(self):
        if self.ei is not None:
            return self.ei
        if self.si is not None:
            return self.si
    status=Column(String())




class TorrentCache(Base):
    __tablename__='torrentcache'
    id=Column(Integer(),primary_key=True)
    time=Column(DateTime,default=datetime.datetime.utcnow())
    hashid=Column(String())
    title=Column(String())
    torrent=Column(String())
    seeders=Column(String())

class CurrentShows(Base):
    __tablename__='currentshows'
    si_id=Column(Integer(),ForeignKey('showinfos.id'),primary_key=True)
    si=relationship("ShowInfo")
    unwatched=Column(Integer())
    eilast_id=Column(Integer(),ForeignKey('episodeinfos.id'))
    eilast=relationship("EpisodeInfo")
    episodes=relationship("CurrentEpisodes")

class EpisodeInfoCache(Base):
    __tablename__='eicache'
    str=Column(String(),primary_key=True)
    eiid=Column(Integer(),ForeignKey("episodeinfos.id"))
    ei=relationship("EpisodeInfo")


class ShowOrderType(Base):
    __tablename__='showtypes'
    si_id=Column(Integer(),ForeignKey('showinfos.id'),primary_key=True)
    si=relationship("ShowInfo")
    type=Column(String())
class Order(Base):
    __tablename__='orders'
    ei1_id=Column(Integer(),ForeignKey('episodeinfos.id'),primary_key=True)
    ei1=relationship("EpisodeInfo", foreign_keys=[ei1_id])
    ei2_id=Column(Integer(),ForeignKey('episodeinfos.id'))
    ei2=relationship("EpisodeInfo", foreign_keys=[ei2_id])

class ShowGroup(Base):
    __tablename__='showgroups'
    si_id=Column(Integer(),ForeignKey('showinfos.id'),primary_key=True)
    si=relationship("ShowInfo")
    group_id=Column(Integer(),ForeignKey('groups.ei_id'))


class TorrentsContent(Base):
    __tablename__='torrentscontent'
    torrent_string=Column(String(),primary_key=True)
    contents=relationship('TorrentsContentEpisodes')
    seeders=Column(Integer())
    title=Column(String())
    def getdict(self):
        return {'title':self.title,'torrent':self.torrent_string,'seeders':self.seeders}


class TorrentsContentEpisodes(Base):
    __tablename__="torrentscontentepisodes"
    id=Column(Integer(),primary_key=True)
    ei_id=Column(Integer(),ForeignKey('episodeinfos.id'))
    ei=relationship('EpisodeInfo')
    tc_torrent_string=Column(String(),ForeignKey('torrentscontent.torrent_string'))
    tc=relationship('TorrentsContent')
    @hybrid_property
    def si(self):
        if self.ei:
            return self.ei.si
        else:
            return None

class Group(Base):
    __tablename__='groups'
    id=Column(Integer())
    si_id=Column(Integer(),ForeignKey('showinfos.id'))
    si=relationship("ShowInfo")
    group_id=Column(Integer())
    ei_id=Column(Integer(),ForeignKey('episodeinfos.id'),primary_key=True)
    ei=relationship("EpisodeInfo")
    line_order=Column(Integer())

class Status(Base):
    __tablename__ = 'statuses'
    id=Column(Integer(),primary_key=True)
    ei_id=Column(Integer(),ForeignKey('episodeinfos.id'))
    ei=relationship('EpisodeInfo')
    si_id=Column(Integer(),ForeignKey('showinfos.id'))
    si=relationship('ShowInfo')
    time=Column(DateTime(),default=datetime.datetime.utcnow())
    status=Column(String())
    @hybrid_property
    def eiorsi(self):
        if self.ei is not None:
            return self.ei
        if self.si is not None:
            return self.si
    @eiorsi.setter
    def eiorsi(self,value):
        if isinstance(value,EpisodeInfo):
            self.ei=value
        if isinstance(value,ShowInfo):
            self.si=value


class EpisodeInfo(Base):
    __tablename__ = 'episodeinfos'
    id=Column(Integer(),primary_key=True)

    seriesname=Column(String())
    lang=Column(String())
    episodenumbers=relationship("EpisodeNumbers")
    seasonnumber = Column(Integer())

    def todict(self):
        ens=map(lambda x:x.episodenumber,self.episodenumbers)
        return {'lang':self.lang,'seriesname':self.seriesname,'seasonnumber':self.seasonnumber,'episodenumbers':ens}

    def compact(self):
        part1 = stc(self.lang, '_ ', self.seriesname, " - S", ('%02d' % self.seasonnumber))

        if len(self.episodenumbers) == 1:
            part2 = ('E%02d' % self.episodenumbers[0].episodenumber)
        elif len(self.episodenumbers)>1:
            part2 = stc('E%02d' % self.episodenumbers[0].episodenumber, '-', 'E%02d' % self.episodenumbers[-1].episodenumber)
        else:
            part2 = ''
        return stc(part1, part2)

    def contains(self, ei):
        if self==ei:
            return True
        if self.lang==ei.lang and self.seriesname==ei.seriesname:
            if self.seasonnumber==ei.seasonnumber:
                self_ens=map(lambda x:x.episodenumber ,self.episodenumbers)
                ei_ens_in_self_ens=map(lambda x:x.episodenumber in self_ens,ei.episodenumbers)
                if all(ei_ens_in_self_ens) :
                    return True
        return False


    def __eq__(self, other):
        if other is None: return False
        if type(self) == type(other):
            return self.compact() == other.compact()
        else:
            return False

    def __hash__(self):
        return (self.lang, self.seriesname, self.seasonnumber, tuple(self.episodenumbers)).__hash__()

    def __str__(self):
        return self.compact()

Base.metadata.create_all()

#import sqlite3

#conn=sqlite3.connect(DBname)
#vals=conn.execute('SELECT * FROM sqlite_master').fetchall()
#import pprint
#pprint.pprint(vals)

pass
