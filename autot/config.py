import os

import smtplib
import tvdb_api
import logging
import configparser
import codecs


def generateblank():
    import StringIO

    output = StringIO.StringIO()
    cp = configparser.ConfigParser()
    cp.add_section('SMTP')
    cp.set('SMTP', 'address', 'smtp.gmail.com')
    cp.set('SMTP', 'port', '587')
    cp.set('SMTP', 'user', '')
    cp.set('SMTP', 'password', '')
    cp.add_section('EMAIL')
    cp.set('EMAIL', 'from','')
    cp.set('EMAIL', 'to','')
    cp.add_section('GENERAL')
    cp.set('GENERAL', 'prefix', os.path.expanduser('~'))
    cp.set('GENERAL', 'seedratio', '4')
    cp.set('GENERAL', 'keepunwatched', '2')
    cp.set('GENERAL', 'candelete', 'False')
    cp.add_section('TVDB_API')
    cp.set('TVDB_API', 'key', '')
    cp.add_section('LANG')
    cp.set('LANG', 'langs', 'en')
    cp.add_section('FEED')
    cp.set('FEED', 'en', '')
    cp.add_section('FILTER')
    cp.set('FILTER', 'filter', 'swesub,720p,1080p,subtitulado,nl subs,.rus.,spanish,german.ws,ws.german,swesub,castellano,sub PT,[ESP]')
    cp.add_section('SEARCH ALT')
    cp.write(output)
    val=output.getvalue()
    output.close()
    return val

#logger = logging.getLogger('AutoTorrent')
#logger.debug('running config.py')
logging.basicConfig()

approot = os.path.realpath(os.path.dirname(os.path.realpath(__file__))) + '/'

#approot=os.path.join(os.path.expanduser('~'),'autot')
#if not os.path.exists(approot):
#    os.mkdir(approot)

conf_file = os.path.join(approot, 'autot.conf')
#if not os.path.exists(os.path.join(approot, 'db')):
#    os.mkdir(os.path.join(approot, 'db'))
#DBname = os.path.join(approot, 'db','autot.db')
cp = configparser.ConfigParser()
cp.read_file(codecs.open(conf_file, "r", "utf8"))

if os.path.exists(conf_file):
    try:
        smtpaddress = cp.get('SMTP', 'address')
        smtpport = cp.getint('SMTP', 'port')
        smtpuser = cp.get('SMTP', 'user')
        smtppassword = cp.get('SMTP', 'password')

        emailreporting = True
    except Exception as e:
        logging.info('No SMTP server, no email reporting')
        emailreporting = False

    try:
        DBurl=cp.get('DB','url')
    except Exception as e:
        logging.info('no connection info for the db')
        exit()


    if not cp.has_option('EMAIL', 'from'):
        logging.info('No from: email address, no email reporting')
        emailreporting = False
    from_addr = cp.get('EMAIL', 'from', fallback='')
    if not cp.has_option('EMAIL', 'to'):
        logging.info('No to: email address, no email reporting')
        emailreporting = False
    to_addr = cp.get('EMAIL', 'to', fallback='')

    dlprefix = cp.get('GENERAL', 'prefix')
    seedratio = cp.getint('GENERAL', 'seedratio')
    keepunwatched = cp.getint('GENERAL', 'keepunwatched')
    candelete = bool(cp.get('GENERAL', 'candelete'))
    tvdbapikey = cp.get('TVDB_API', 'key')
    tvdb_instance = {}
    feed_urls={}
    langs = cp.get('LANG', 'langs').split(',')
    for l in langs:
        if not os.path.exists(os.path.join(approot,'cache','tvdb')):
            os.mkdir(os.path.join(approot,'cache'))
            os.mkdir(os.path.join(approot,'cache','tvdb'))
        tvdb_instance[l] = tvdb_api.Tvdb(apikey=tvdbapikey, language=str(l[0:2]),cache=os.path.join(approot,'cache','tvdb'))
        feed_urls[l]=cp.get('FEED', l, fallback=None)
    globalfilter = cp.get('FILTER', 'filter').split(',')

    if tvdbapikey == '':
        logging.critical("No TVDB API key, better with one (not using tvdapi's key")
        exit()


def setdata(nameplug, data):
    assert (data is not None)
    cp = configparser.ConfigParser()
    cp.read_file(codecs.open(conf_file, "r", "utf8"))
    if not cp.has_section(nameplug): cp.add_section(nameplug)
    for v in data:
        cp.set(nameplug, v, value=unicode(data[v]))
    fp=codecs.open(conf_file, "w", "utf8")
    cp.write(fp)
    fp.close()


def getdata(nameplugin):
    cp = configparser.ConfigParser()
    cp.read_file(codecs.open(conf_file, "r", "utf8"))
    data = {}
    if cp.has_section(nameplugin):
        opts = cp.options(nameplugin)
        for o in opts:
            data[o] = cp.get(nameplugin, o)
            try:
                data[o] = int(data[o])
            except ValueError:
                pass
    return data