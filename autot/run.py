#!/usr/bin/env python
# -*- coding=utf-8
from sqlalchemy.sql.expression import func, and_
import sys
import hashlib
import time
import shutil
from tvnamer.tvnamer_exceptions import *
from transmissionrpc import TransmissionError
import traceback
import psutil
import tvdb_exceptions
from tvnamer.tvnamer_exceptions import UserAbort
import tvnamer.config

from finisheddownloads import FinishedDownloadsHandler, FinishedFilesEventHandler
from orderkeeper import KeepOrders
from parser import *
from sqlA import ShowInfo, CurrentEpisodes
from sqlA import CurrentShows, Status
import plexint
import utils
from search import TorrentSearcher
from torrentint import TorrentInteraction
from utils import stc
from store import getvalue, setvalue
import store
from autot import Session
from sqlA import EpisodeInfo, Order


tvnamer.config.Config['select_first'] = False
tvnamer.config.Config['titlecase_filename'] = True
tvnamer.config.Config['search_all_languages'] = False
tvnamer.config.Config['verbose'] = True
# show name Season 01 Episode 20
tvnamer.config.Config['filename_patterns'].insert(0,
                                                  '''^(?P<seriesname>.+?)[ ]?
                                              [Ss]eason[ ]?(?P<seasonnumber>[0-9]+)[ ]?
                                              [Ee]pisode[ ]?(?P<episodenumberstart>\d+)[ &]+(?P<episodenumberend>\d+)
                                              [^\\/]*$''')
ONERUNLOG = []
from config import *

import zmq

sys.stdout = codecs.getwriter('utf8')(sys.stdout)
# session=Session()

def downloading_episodes(si):
    session = Session.object_session(si)
    return map(lambda x: x.ei, session.query(TorrentStatus).filter(
        and_(TorrentStatus.si == si, TorrentStatus.status == 'Downloading')).all())


def send_email_episode(ei, filename, title):
    session = Session.object_session(ei)
    si = store.getsi(ei)
    store.addifnotexists(session, Task, si=si, ei=ei,
                         task='Added apparent episode file to downloads: File Named:' + filename)

    msg = ('\n' + ('<+>' * 20) + '\n' + stc('Added torrent: ', ei) + '\n'
           + '   File named: ' + filename
           + '\n' + '    Torrent named: ' + title + '\r\n').encode('utf-8')

    sendmail(msg)

    logger.info(msg)


class AutoTRun:
    def __init__(self):
        self.plex_interaction = plexint.PlexInteraction()
        self.torrent_interaction = TorrentInteraction()
        self.orderkeeper = KeepOrders()
        self.searcher = TorrentSearcher()
        if not os.path.exists(approot + '/db'):
            os.makedirs(approot + '/db')

    def isepisode_downloading(self, ei):
        session = Session.object_session(ei)
        logger.debug(stc(ei, ' Trying to find if downloading'))
        breakout = False
        for tor in self.torrent_interaction.transmissionserver.get_torrents():
            if breakout: continue
            breakout = False
            tid = tor.id
            files = self.torrent_interaction.transmissionserver.get_files(tid)
            if not files.has_key(tid): continue
            for i, filetor in files[tid].iteritems():
                try:
                    directory = tor._fields['downloadDir'].value
                    stringfilename = os.path.basename(files[tid][i]['name'])
                    # logger.debug('test  '+stringfilename)
                    if directory[-1] == '/':
                        directory = directory[0:-1]
                    spl = os.path.basename(directory).split('_')
                    if len(spl) != 2:
                        toparse = exceptions_fromdl(stringfilename)
                        bools = []
                        for lang in langs:
                            bools.append(parse_episodeinfo(toparse, lang[0:2]) is not None, session=session)
                        if True in bools:
                            # Probably TV Show episode that is not in the proper directory
                            logger.error(stc("need to change the location of the torrent ", tor._get_name_string()))
                            breakout = True
                            continue
                        else:
                            # Completely non TV Show related download, don't do anything
                            pass
                        continue
                    lang = spl[1]
                    if lang == 'nonAuto':
                        # Excluded downloads (non-Plex destined)
                        continue
                    if lang != ei.lang:
                        # Apparently not the correct language
                        continue
                    toparse = exceptions_fromdl(stringfilename)
                    episodeinfo = parse_episodeinfo(toparse, lang, session=session)
                    if episodeinfo is None:
                        continue

                    if episodeinfo.contains(ei):
                        logger.debug(stc(stringfilename, ' identifies with ', ei))
                        if tor.status == 'stopped':
                            store.addifnotexists(session, Task, task=stc('Unpaused the torrent ', ei))
                            self.torrent_interaction.transmissionserver.start_torrent(tid)
                        if not files[tid][i]['selected']:
                            store.addifnotexists(session, Task, ei=ei, task='Selected file in a current torrent')
                            files[tid][i]['selected'] = True
                            self.torrent_interaction.transmissionserver.set_files(files)
                            self.checkquota_beforelaunching(tid, episodeinfo)

                        logger.info('Found a file that is downloading')

                        if not 'downloading' in session.query(Status.status).filter(Status.ei == ei).all():
                            store.get_one_or_create(session, Status, ei=ei, status='downloading')
                            send_email_episode(ei, files[tid][i]['name'],
                                               stc('[Externally added] :', tor._get_name_string()))
                        return True
                        # else:
                        # logger.debug(stc('non recognizable file ',toparse))
                except tvnamer.tvnamer_exceptions.BaseTvnamerException:
                    # logger.debug(stc('non parseable file ',files[tid][i]['name']))
                    pass
        logger.debug(stc('Status ', ei, ' not downloading'))
        if session.query(Status.status).filter(Status.ei == ei).all() == ['downloading']:
            session.query(Status.status).filter(Status.ei == ei).delete()
        return False

    def checkquota_beforelaunching(self, tid, episodeinfo):
        session = Session.object_session(episodeinfo)
        import psutil

        usage = psutil.disk_usage('/')
        size = self.torrent_interaction.transmissionserver.get_torrent(tid)._fields['sizeWhenDone'].value
        if 2 * size > usage.free:
            self.torrent_interaction.transmissionserver.stop_torrent(tid)
            store.addifnotexists(session, Status, ei=episodeinfo, status='disk is too full')
            logger.error('Disk is full')

    def global_filter(self, g):
        return all([f.lower() not in g['title'].lower() for f in globalfilter])

    def analyzefromget(self, get, goal_ei):
        lang = goal_ei.lang
        if get:
            get = [tup for tup in get if self.global_filter(tup)]
        if get:
            def getkey(item):
                return item['seeders']

            seen = set()
            result = []
            for d in get:
                h = tuple(d['torrent'])
                if h not in seen:
                    result.append(d)
                    seen.add(h)

            newget = sorted(result, key=getkey, reverse=True)

            self.analyze(newget, lang)

    def tryfromget(self, get, eiorsi):
        session = Session.object_session(eiorsi)
        if get:
            get = [tup for tup in get if self.global_filter(tup)]
        if get:
            def getkey(item):
                return (parse_episodeinfo(item['title'], eiorsi.lang, session=session) == eiorsi, item['seeders'])

            newget = sorted(get, key=getkey, reverse=True)
            if self.put_episode(newget, eiorsi):
                store.addifnotexists(session, Status, eiorsi=eiorsi, status='downloading')
                return True
        return None

    def dosearches(self):
        while 1:
            session=Session()
            try:
                sq = session.query(SearchQuery).order_by(SearchQuery.priority.desc()).first()
            except NoResultFound:
                print('No more SQs found')
                break
            print(stc(sq.data, ' ', sq.eiorsi))
            goal_eiorsi = sq.eiorsi
            lowerseeds = []
            if isinstance(goal_eiorsi, EpisodeInfo):
                tce = session.query(TorrentsContentEpisodes).filter(TorrentsContentEpisodes.ei == goal_eiorsi).all()
            elif isinstance(goal_eiorsi, ShowInfo):
                tce = session.query(TorrentsContentEpisodes).filter(TorrentsContentEpisodes.si == goal_eiorsi).all()
            get = []
            if not tce == []:
                for t in tce:
                    if t.tc_torrent_string is None or t.tc is None:
                        session.delete(t)
                        continue
                    getd = t.tc.getdict()
                    test = all(map(lambda x: x is not None, getd.values()))
                    if test:
                        get.append(getd)
                    else:
                        session.delete(t)
                        session.commit()

            gotfrom = self.searcher.getfrom(sq, cache=True)
            if gotfrom:
                get.extend(gotfrom)
            found = False
            if get and not get == []:
                if sq.getit:
                    if self.tryfromget(get, goal_eiorsi):
                        found = True
                        for sq2 in session.query(SearchQuery).filter(SearchQuery.eiorsi == sq.eiorsi).all():
                            sq2.getit = False
                            sq2.priority = 0
                    else:
                        if session.query(SearchQuery).filter(SearchQuery.eiorsi == goal_eiorsi).count() == 0:
                            if not 'couldnotfind' in session.query(Status).filter(Status.ei == goal_eiorsi).all():
                                if isinstance(goal_eiorsi, EpisodeInfo):
                                    store.addifnotexists(session, Task, ei=goal_eiorsi,
                                                         task='could not find new episode')
                                elif isinstance(goal_eiorsi, ShowInfo):
                                    store.addifnotexists(session, Task, si=goal_eiorsi,
                                                         task='could not find new episode')
                            store.addifnotexists(session, Status, eiorsi=goal_eiorsi, status='couldnotfind')
                        session.delete(sq)
                        session.commit()
                else:
                    if self.analyzefromget(get, goal_eiorsi):
                        found = True
                pass
            if found:
                pass
            session.delete(sq)

    def get_episode(self, episodeinfotoget):
        session = Session.object_session(episodeinfotoget)
        assert isinstance(episodeinfotoget, EpisodeInfo)
        logger.msg(stc('Wanting ', episodeinfotoget))
        # logger.info('Wanting episode '+Compact(EItoget))
        # name = compact_ei(episodeinfotoget)
        if not (self.plex_interaction.isepisode_inplex(episodeinfotoget)
                or self.isepisode_downloading(episodeinfotoget)):
            if ('couldnotfind' in session.query(Status.status).filter(Status.ei == episodeinfotoget).all()) and (
                    not '-f' in sys.argv):
                logger.debug('could not find it previously, not searching again')
                return
            store.addifnotexists(session, Task, ei=episodeinfotoget, task='needed new episode for',
                                 si=store.getsi(episodeinfotoget))

            data = {}
            data['lang'] = episodeinfotoget.lang

            self.searcher.add_searchqueries_episode(episodeinfotoget)
        else:
            logger.debug(stc("Wanted ", episodeinfotoget, ' in Plex or Currently downloading'))

    def analyze(self, gets, lang):
        session=Session()
        with session.begin():
            pre = len(self.torrent_interaction.transmissionserver.get_torrents())

            dir = dlprefix + lang
            foundit = False
            for getitem in gets:
                torrent_string = getitem['torrent']
                if session.query(TorrentsContent).filter(
                                TorrentsContent.torrent_string == torrent_string).count() != 0: continue

                try:
                    added_torrent = self.torrent_interaction.transmissionserver.add_torrent(torrent=torrent_string,
                                                                                            download_dir=dir)

                except TransmissionError:
                    logger.info('The torrent seems invalid')
                    continue
                tid = added_torrent.id
                nt = 0
                logger.debug('waiting metadata...')
                cancelthat = False
                while self.torrent_interaction.transmissionserver.get_files(tid)[tid] == {}:
                    time.sleep(1)
                    nt = nt + 1
                    if nt > 20:
                        if self.torrent_interaction.transmissionserver.get_torrent(tid)._fields[
                            'peersConnected'].value == 0:
                            logger.debug('No peers for metadata')
                            cancelthat = True
                            break
                    if nt > 100:
                        logger.debug('Waited too long for metadata')
                        cancelthat = True
                        break
                if cancelthat:
                    store.addifnotexists(session, TorrentsContent, torrent_string=torrent_string,
                                         seeders=getitem['seeders'], title=getitem['title'])

                    # store.addifnotexists(session,TorrentsContent,torrent_string=torrent_string,contents=[None],seeders=getitem['seeders'],title=getitem['title'])
                    continue
                    # possible race condition, allocated files before we can stop it?
                self.torrent_interaction.transmissionserver.stop_torrent(tid)

                post = len(self.torrent_interaction.transmissionserver.get_torrents())
                newtorrent = post > pre
                files = self.torrent_interaction.transmissionserver.get_files(tid)
                foundit = False
                if newtorrent:
                    for i, file in files[tid].iteritems():
                        files[tid][i]['selected'] = False
                self.torrent_interaction.transmissionserver.set_files(files)
                valid = False
                for i, file in files[tid].iteritems():
                    try:
                        filename = os.path.basename(files[tid][i]['name'])
                        logger.debug('Torrent content: ' + filename)
                        ei2 = parse_episodeinfo(filename, lang, session=session)
                        if ei2:
                            valid = True
                            logger.debug(stc('Parsed to: ', ei2))

                            size = self.torrent_interaction.transmissionserver.get_torrent(tid)._fields[
                                'sizeWhenDone'].value
                            if size < 1000000: continue
                            tce = store.get_one_or_create(session, TorrentsContentEpisodes, ei=ei2)

                            if session.query(TorrentsContent).filter(
                                            TorrentsContent.torrent_string == torrent_string).count() == 0:
                                # tc=TorrentsContent()
                                tc = store.get_one_or_create(session, TorrentsContent, torrent_string=torrent_string,
                                                             seeders=getitem['seeders'], title=getitem['title'])
                                tc.contents = [tce]
                                # store.addifnotexists(session,TorrentsContent,torrent_string=torrent_string,contents=tce,seeders=getitem['seeders'],title=getitem['title'])
                                # session.add(tc)
                                # session.commit()
                            else:
                                tc = session.query(TorrentsContent).filter(
                                    TorrentsContent.torrent_string == torrent_string).one()
                                tc.contents.append(tce)
                            pass
                    except InvalidFilename:
                        # not a recognized file
                        pass  # logger.debug("non recognizable file " + files[tid][i]['name'])
                self.torrent_interaction.transmissionserver.remove_torrent(tid, delete_data=True)
            return

    def put_episode(self, gets, eiorsi, puttodownload=True):
        session = Session.object_session(eiorsi)
        if eiorsi is EpisodeInfo:
            israndom = self.orderkeeper.israndom(store.getsi(eiorsi))
        else:
            israndom = self.orderkeeper.israndom(eiorsi)
        pre = len(self.torrent_interaction.transmissionserver.get_torrents())

        dir = dlprefix + eiorsi.lang
        foundit = False
        for getitem in gets:
            torrent_string = getitem['torrent']
            # tcc=session.query(TorrentsContent).filter(TorrentsContent.id==get).all()

            if israndom:
                logger.debug(stc('Trying to find a random episode for ', eiorsi, ' in ', getitem['title']))
            else:
                logger.debug(stc('Trying to find the episode ', eiorsi, ' in ', getitem['title']))

            try:
                added_torrent = self.torrent_interaction.transmissionserver.add_torrent(torrent=torrent_string,
                                                                                        download_dir=dir)
            except (TransmissionError, TypeError, RuntimeError):
                logger.info('The torrent seems invalid')
                continue
            tid = added_torrent.id
            nt = 0
            logger.debug('waiting metadata...')
            cancelthat = False
            while self.torrent_interaction.transmissionserver.get_files(tid)[tid] == {}:
                time.sleep(1)
                nt = nt + 1
                if nt > 20:
                    if 'peersConnected' in added_torrent._fields:
                        if added_torrent._fields['peersConnected'].value == 0:
                            logger.debug('No peers for metadata')
                            cancelthat = True
                            break
                if nt > 100:
                    logger.debug('Waited too long for metadata')
                    cancelthat = True
                    break

            # possible race condition, allocated files before we can stop it?
            self.torrent_interaction.transmissionserver.stop_torrent(tid)

            if cancelthat:
                self.torrent_interaction.transmissionserver.remove_torrent(tid, delete_data=True)
                continue

            usage = psutil.disk_usage('/')

            post = len(self.torrent_interaction.transmissionserver.get_torrents())
            newtorrent = post > pre
            files = self.torrent_interaction.transmissionserver.get_files(tid)
            foundit = False
            if newtorrent:
                for i, file in files[tid].iteritems():
                    files[tid][i]['selected'] = False
            self.torrent_interaction.transmissionserver.set_files(files)
            episodeinfos = []
            valid = False
            for i, file in files[tid].iteritems():
                try:
                    filename = os.path.basename(files[tid][i]['name'])
                    logger.debug('Torrent content: ' + filename)
                    ei2 = parse_episodeinfo(filename, eiorsi.lang, session=session)
                    if ei2:
                        episodeinfos.append(ei2)
                        # ei_name = compact_ei(ei)

                        logger.debug(stc('Parsed to: ', ei2))
                    else:
                        logger.debug("non recognizable file " + filename)
                        episodeinfos.append(None)
                except InvalidFilename:
                    # not a recognized file
                    logger.debug("non recognizable file " + files[tid][i]['name'])
            for i, file in files[tid].iteritems():
                filename = os.path.basename(files[tid][i]['name'])
                ei2 = episodeinfos[i]
                if ei2:
                    if not israndom:
                        ei2ns = map(lambda x: x.episodenumber, ei2.episodenumbers)
                        eins = map(lambda x: x.episodenumber, eiorsi.episodenumbers)
                        containsei = all(map(lambda y: y in ei2ns, eins)) and eiorsi.seasonnumber == ei2.seasonnumber
                        if not containsei:
                            continue
                    if israndom:
                        if not eiorsi.seriesname == ei2.seriesname: continue
                    size = self.torrent_interaction.transmissionserver.get_torrent(tid).files()[i]['size']
                    if size < 1000000:
                        continue

                    # got our file!
                    if not files[tid][i]['selected']:
                        files[tid][i]['selected'] = True
                    self.torrent_interaction.transmissionserver.set_files(files)
                    self.checkquota_beforelaunching(tid, ei2)

                    # title = self.torrent_interaction.transmissionserver.get_torrent(tid)._get_name_string()

                    if self.torrent_interaction.transmissionserver.get_torrent(tid).status == 'stopped':
                        if puttodownload:
                            self.torrent_interaction.transmissionserver.start_torrent(tid)
                        else:
                            self.torrent_interaction.transmissionserver.remove_torrent(tid, delete_data=True)
                    logger.info(stc('Put to download: ', getitem['title']))

                    send_email_episode(eiorsi, filename, getitem['title'])
                    store.addifnotexists(session, Status, eiorsi=eiorsi, status='downloading')
                    return True
                    # if not foundit:
                    # ei2 = parse_episodeinfo(getitem['title'], eiorsi.lang)
            # if ei2:
            # ei2ns=map(lambda x:x.episodenumber,ei2.episodenumbers)
            # eins=map(lambda x:x.episodenumber,eiorsi.episodenumbers)
            # containsei=all(map(lambda y: y in ei2ns,eins))
            # if containsei:
            # #the title of the torrent matches. Download everything
            # if self.torrent_interaction.transmissionserver.get_torrent(tid).status == 'stopped':
            # self.torrent_interaction.transmissionserver.start_torrent(tid)
            # #title = self.torrent_interaction.transmissionserver.get_torrent(tid)._get_name_string()
            # logger.info(stc('Put to download: ', getitem['title']))
            # self.send_email_episode(eiorsi, '', getitem['title'])
            # return True
            logger.debug("not found in there")
            self.torrent_interaction.transmissionserver.remove_torrent(tid, delete_data=True)
        return

    def geteilastep(self, si_in):
        session = Session.object_session(si_in)
        try:
            return session.query(CurrentShows).filter(CurrentShows.si == si_in).one().eilast
        except NoResultFound:
            return None

    def get_episode_from_feed(self):
        session = Session()
        previousids = []
        for lang in langs:
            import feedparser

            if not feed_urls[lang]: continue
            feed = feedparser.parse(feed_urls[lang])
            for entry in feed['entries']:
                id = entry['id']
                link = entry['links'][0]['href']
                if session.query(SeeninRSS).filter(SeeninRSS.id == entry['id']).count() == 0:
                    try:
                        episodeinfo = parse_episodeinfo(entry['title'], lang, session=session)
                        if episodeinfo:
                            si = store.getsi(episodeinfo)
                            downloadingandunwatched_episodes = self.downloadingandunwatched_episodes(si)
                            eilast = self.geteilastep(si)
                            if eilast is None:
                                # This show hasn't ever been seen?
                                pass
                            else:
                                if self.orderkeeper.getnext_order(eilast) == episodeinfo:
                                    numunwatched = len(downloadingandunwatched_episodes)
                                    if numunwatched < keepunwatched \
                                            and not self.plex_interaction.isepisode_inplex(
                                                    episodeinfo) and not self.isepisode_downloading(episodeinfo):
                                        store.addifnotexists(session, Task, ei=episodeinfo,
                                                             task='found a suitable episode in RSS feed')
                                        addedtorrent = self.torrent_interaction.transmissionserver.add_torrent(link)
                                        send_email_episode(episodeinfo, '', entry['title'])
                    except tvnamer.tvnamer_exceptions.InvalidFilename:
                        print(entry['title'] + ' not recognized')
                    store.addifnotexists(session, SeeninRSS, id=entry['id'])


    def get_username(self):
        import pwd

        return pwd.getpwuid(os.getuid())[0]

    def resetdb(self, model):
        session = Session()
        try:
            session.query(model).delete()
            session.commit()
        except:
            session.rollback()

    def treatspace(self):
        session = Session()
        session.query(Space).delete()
        session.flush()
        import psutil

        sizedl = utils.get_size(os.path.expanduser("~/downloads"))
        sizePlex = utils.get_size(os.path.expanduser("~/Plex"))


        # utils.setspace('Other','var',utils.get_size('/var'))
        # utils.setspace('Other','usr',utils.get_size('/usr'))
        # utils.setspace('Other','dev',utils.get_size('/dev'))
        usage = psutil.disk_usage('/')

        store.setspace(parent_id='', id='root', size=usage.total)
        store.setspace(parent_id='root', id='Used', size=usage.percent * usage.total / 100)
        store.setspace(parent_id='Used', id='Other', size=usage.used - sizePlex - sizedl)
        store.setspace(parent_id='Used', id='Plex', size=sizePlex)
        store.setspace(parent_id='Used', id='downloads', size=sizedl)
        store.setspace(parent_id='root', id='Free', size=usage.free)

        for lang in langs:
            pathlang = os.path.expanduser("~/downloads/finished_" + lang)
            sizepathlang = utils.get_size(pathlang)
            store.setspace(parent_id='downloads', id='finished_' + lang, size=sizepathlang)
        pass

    def clearrunning(self):
        setvalue('running', 'False')
        exit()

    def downloadingandunwatched_episodes(self, si):
        session = Session.object_session(si)
        gs = self.orderkeeper.getgroups()
        m = map(lambda x: x.si, gs)
        matches = []
        if si in m:
            lineordermax = 0
            for si_1 in m:
                unwatched_episodes = self.currentepisodes(si_1, 'fully unwatched')
                matches.extend(downloading_episodes(si))
                matches.extend(unwatched_episodes)
            Q = session.query(func.max(Group.line_order).label("maxlineorder"))
            matches_ei_id = map(lambda x: x.id, matches)
            if not matches_ei_id == []:
                result = Q.filter(and_(Group.ei_id.in_(matches_ei_id))).one()
                lineordermax = max((result.maxlineorder, lineordermax))
                ei_last = session.query(Group).filter(Group.line_order == lineordermax).first().ei
            else:
                ei_last = None
        else:
            ei_last = self.geteilastep(si)
            unwatched_episodes = self.currentepisodes(si, 'fully unwatched')
            matches.extend(downloading_episodes(si))
            matches.extend(unwatched_episodes)
        return list(set(matches)), ei_last

    def treat_downloaded_file(self, filematch, lang):
        session = Session()
        # logger.debug('Treating '+filematch)
        haschanged = False
        base = os.path.basename(filematch)
        dire = os.path.dirname(filematch)

        season = ("Saison" if lang == 'fr' else "Season")

        try:
            if base.endswith('.part'): return
            try:
                siize = os.path.getsize(filematch)
                if not os.path.exists(filematch): return
            except OSError:
                return
            if os.path.getsize(filematch) > 20000000:
                episodeinfo = parse_episodeinfo(exceptions_fromdl(base), lang, session=session)
                if episodeinfo is None:
                    logger.debug(stc('file ', filematch, 'doesn''t match an episode'))
                    return
                    # compactepisodeinfo = compact_ei(episodeinfo)

                newdir = os.path.expanduser(
                    "~/Plex/" + lang + "_Shows/" + episodeinfo.seriesname + "/" + season + " " + str(
                        episodeinfo.seasonnumber))
                if not os.path.exists(newdir):
                    os.makedirs(newdir, mode=0777)

                compnolang = ''.join(episodeinfo.compact().split('_')[1:]).strip()
                newfile = newdir + "/" + compnolang + '_' + base
                if not os.path.exists(newfile):
                    logger.info(stc(episodeinfo, ' Treating downloaded file'))
                    logger.info(stc('Moving ', filematch))
                    logger.info(stc('to ', newfile))

                    shutil.move(filematch, newfile)
                    os.link(newfile, filematch)
                    store.addifnotexists(session, Task, ei=episodeinfo, task='Added to Plex from')
                    haschanged = True
                    store.addifnotexists(session, Status, ei=episodeinfo, status='in Plex')
                    store.removefromstatus(episodeinfo, 'downloading')
            if haschanged:
                self.plex_interaction.refresh_plex()

        except tvnamer.tvnamer_exceptions.InvalidFilename:
            logger.debug(stc('File not recognized ', base))

    def treat_downloaded_files(self):
        for lang in langs:
            FinishedDownloadsHandler(lang).treat_downloaded_files()

    def currentepisodes(self, si, type_viewed_status='*'):
        session = Session.object_session(si)
        if type_viewed_status == '*':
            return map(lambda x: x.ei, session.query(CurrentEpisodes).filter(CurrentEpisodes.si == si).all())
        else:
            return map(lambda x: x.ei, session.query(CurrentEpisodes).filter(
                and_(CurrentEpisodes.si == si, CurrentEpisodes.type == type_viewed_status)).all())

    def treat_nextepisodes_shows(self, si):
        session = Session.object_session(si)
        eps, ei_last = self.downloadingandunwatched_episodes(si)
        if len(eps) < keepunwatched and session.query(SearchQuery).filter(SearchQuery.eiorsi == si).count() == 0:
            try:
                if self.orderkeeper.israndom(si):
                    self.searcher.add_searchqueries_random(si)
                if ei_last:
                    no = self.orderkeeper.getnext_order(ei_last)
                    if no:
                        episodeinfo = no
                    else:
                        episodeinfo = self.plex_interaction.getnextep(ei_last)
                    if not episodeinfo:
                        # A finished series or an unfinished series with not yet published seasons
                        return
                    logger.debug(
                        stc('Next wanted Episode after ', ei_last, ' is ', episodeinfo))
                    for en in map(lambda x: x.episodenumber, episodeinfo.episodenumbers):
                        ep = tvdb_instance[episodeinfo.lang][episodeinfo.seriesname][episodeinfo.seasonnumber][en]
                        if ep['firstaired']:
                            dtep = datetime.datetime.strptime(ep['firstaired'], '%Y-%m-%d')
                            dtnow = datetime.datetime.now()
                            dt = datetime.timedelta(days=(5 if '-' in episodeinfo.lang else 1))
                            if (dtnow - dtep) > dt:
                                logger.debug(stc('Wanting ', episodeinfo))
                                if session.query(SearchQuery).filter(SearchQuery.eiorsi == episodeinfo).count() == 0:
                                    self.get_episode(episodeinfo)
                                setvalue('doing', '')
                            else:
                                logger.debug(
                                    stc(episodeinfo, ' not yet old enough to be probably available on torrent'))
                        else:
                            logger.debug(stc('Couldn\'t find the aired date of the wanted episode ', episodeinfo))
            except tvdb_exceptions.tvdb_shownotfound, tvdb_exceptions.tvdb_seasonnotfound:
                logger.error(stc('Couldn\'t find TV show ', si.seriesname, ', extracted from Plex in tvdb'))

    def loopshows(self):
        session = Session()
        for si in map(lambda x: x.si, session.query(CurrentShows).all()):
            self.treat_nextepisodes_shows(si)
        for ce in map(lambda x: x.episodes, session.query(CurrentShows).all()):
            pass

    def main(self):
        session = Session()
        # logger.debug('Apparently running as ' + self.get_username())

        # store.testschemas()
        if '-resetdb' in sys.argv:
            from sqlalchemy import inspect

            inspector = inspect(engine)
            for table_name in inspector.get_table_names():
                if table_name in sys.argv:
                    self.resetdb(table_name)

        if '-clearrunning' in sys.argv:
            self.clearrunning()
        if '-isrunning' in sys.argv:
            print(getvalue('running'))
            exit()
        if '-treatorder' in sys.argv or session.query(Order).count() == 0:
            self.orderkeeper.treatorder()

    def loop(self):
        print(engine)
        while 1:
            logger.msg('Treating finished files')
            self.treat_downloaded_files()
            logger.msg('Reading Space')
            self.treatspace()
            logger.msg('Looping Plex')
            self.plex_interaction.loop_plex()
            logger.msg('Looping Torrents')
            self.torrent_interaction.treat_downloading_episodes()

            logger.msg('Looping Currently Watching Shows')
            self.loopshows()

            logger.msg('Cleaning finished-to-seed torrents')
            self.torrent_interaction.remove_seeded_torrents()
            logger.msg('Cleaning cache')
            self.searcher.cleaningcache()
            logger.msg('Removing  bad torrent temporary files')
            self.torrent_interaction.remove_badpartfiles()
            logger.msg('Getting episodes from RSS')
            self.get_episode_from_feed()
            logger.msg('Doing searches')
            self.dosearches()
            setvalue('lastsuccess', str(time.time()))
            setvalue('running', 'False')
            logger.msg('Finished')
            time.sleep(240)


from sqlA import Group


def setup_logger():
    logger = logging.getLogger('AutoTorrent')

    logger.setLevel(logging.DEBUG)

    level_msgnum = 25
    logging.addLevelName(level_msgnum, "MSG")

    def log_msg(self, message, *args, **kws):
        # Yes, logger takes its '*args' as 'args'.
        if self.isEnabledFor(level_msgnum):
            self._log(level_msgnum, message, args, **kws)

    logging.Logger.msg = log_msg


def sendmail(msg):
    while True:
        try:
            smtpserver = smtplib.SMTP(smtpaddress, smtpport)
            smtpserver.ehlo()
            smtpserver.starttls()
            smtpserver.ehlo()
            smtpserver.login(smtpuser, smtppassword)
            smtpserver.sendmail(from_addr, to_addr, msg)
            smtpserver.quit()
            break
        except Exception, e:
            if 'quota exceeded' in e.smtp_error:
                return
            time.sleep(30)
    return


from watchdog.observers import Observer


inst = AutoTRun()
logger = logging.getLogger('AutoTorrent')


def dostuff():
    logging.basicConfig()
    event_handlers = {}
    observer = Observer()
    for lang in langs:
        event_handlers[lang] = FinishedFilesEventHandler(lang)
        observer.schedule(event_handlers[lang], os.path.dirname(dlprefix), recursive=True)
        # observer.schedule(event_handler, path2, recursive=True)
    observer.start()
    setup_logger()

    inst.loop()


if __name__ == "__main__":
    if '-d' in sys.argv:
        # direct debugging
        print('running main')
        dostuff()
    else:
        try:
            dostuff()
        except Exception as e:
            MSG = '\n' + ('<!>' * 20) + '\nCrashed ! '
            if e.__doc__:
                MSG += 'Exception doc: ' + e.__doc__ + '\n'
            if e.message:
                MSG += 'Exception message: ' + str(e.message) + '\n'
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            MSG += 'Exception info:\n' + ''.join('!! ' + line for line in lines)
            logger.info(MSG)
            MSG2 = ''.join('\n<><> ' + line for line in ONERUNLOG)
            print(MSG + MSG2)
            if emailreporting:
                import hashlib

                HASH = hashlib.md5(MSG).hexdigest()
                if getvalue('lastrunhash') != HASH:
                    # re-send email only if the error isn't the same as last run
                    sendmail(MSG + MSG2)
                setvalue('lastrunhash', HASH)
